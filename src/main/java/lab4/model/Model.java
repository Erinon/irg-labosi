package lab4.model;

public class Model {

    Polygon polygon;
    private int width;
    private int height;
    private boolean konveksnost;
    private boolean popunjavanje;
    private boolean state1;

    public Model(int width, int height) {
        this.width = width;
        this.height = height;
        this.konveksnost = false;
        this.popunjavanje = false;
        this.state1 = true;
        this.polygon = new Polygon();
    }

    public void addVertex() {
        if (konveksnost && !polygon.isConvex()) {
            System.out.println("Vrh se ne prihvaća.");

            return;
        }

        polygon.addVertex();
    }

    public void updateMouse(int x, int y) {
        polygon.updateMouse(x, y);
    }

    public void testPoint(int x, int y) {
        if (!polygon.isConvex()) {
            System.out.println("Ispitivanje točke nije moguće.");

            return;
        }

        polygon.testPoint(x, y);
    }

    public void toggleKonveksnost() {
        if (!state1 || !konveksnost && !polygon.isConvex()) {
            System.out.println("Promjena zastavice nije moguća.");

            return;
        }

        konveksnost ^= true;
    }

    public void togglePopunjavanje() {
        if (!state1) {
            System.out.println("Promjena zastavice nije moguća.");

            return;
        }

        popunjavanje ^= true;
    }

    public void toggleState() {
        if (state1 && polygon.getSize() < 3 || state1 && konveksnost && !polygon.isConvex()) {
            System.out.println("Promjena stanja nije moguća.");

            return;
        }

        this.konveksnost = false;
        this.popunjavanje = false;

        if (!state1) {
            this.polygon = new Polygon();
        }

        state1 ^= true;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Polygon getPolygon() {
        return polygon;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public boolean isKonveksnost() {
        return konveksnost;
    }

    public boolean isPopunjavanje() {
        return popunjavanje;
    }

    public boolean isState1() {
        return state1;
    }

}
