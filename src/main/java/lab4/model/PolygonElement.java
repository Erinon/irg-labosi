package lab4.model;

public class PolygonElement {

    private Point vertex;
    private Edge edge;
    private boolean left;

    public PolygonElement() {
        this.vertex = new Point();
        this.edge = new Edge();
    }

    private PolygonElement(Point vertex, Edge edge, boolean left) {
        this.vertex = vertex;
        this.edge = edge;
        this.left = left;
    }

    public Point getVertex() {
        return vertex;
    }

    public Edge getEdge() {
        return edge;
    }

    public boolean isLeft() {
        return left;
    }

    public void setLeft(boolean left) {
        this.left = left;
    }

    public PolygonElement copy() {
        return new PolygonElement(vertex.copy(), edge.copy(), left);
    }

}
