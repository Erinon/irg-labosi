package lab4.model;

import java.util.LinkedList;
import java.util.List;

public class Polygon {

    private List<PolygonElement> elements;
    private boolean convex;
    private boolean clockwise;
    private int size;

    public Polygon() {
        this.elements = new LinkedList<>();
        this.elements.add(new PolygonElement());
        this.size = 1;
        this.convex = true;
    }

    public void addVertex() {
        elements.add(elements.get(size - 1).copy());
        size++;
    }

    public void updateMouse(int x, int y) {
        elements.get(size - 1).getVertex().setX(x);
        elements.get(size - 1).getVertex().setY(y);

        if (size > 1) {
            updateConvexAndOrientation();

            updateEdges();
        }
    }

    public void testPoint(int x, int y) {
        boolean onEdge = false;

        for (int i = 0; i < size; i++) {
            if ((elements.get(i).getEdge().getA() * x +
                    elements.get(i).getEdge().getB() * y +
                    elements.get(i).getEdge().getC() < 0) ^ clockwise) {
                System.out.println("Točka je izvan poligona.");

                return;
            }

            if (elements.get(i).getEdge().getA() * x +
                    elements.get(i).getEdge().getB() * y +
                    elements.get(i).getEdge().getC() == 0) {
                onEdge = true;
            }
        }

        if (onEdge) {
            System.out.println("Točka je na rubu poligona.");
        } else {
            System.out.println("Točka je unutar poligona.");
        }
    }

    private void updateConvexAndOrientation() {
        int i, i0, r;
        int iznad, ispod, na;

        ispod = iznad = na = 0;

        i0 = size - 2;
        for (i = 0; i < size; i++, i0++) {
            if (i0 >= size) {
                i0 = 0;
            }

            r = elements.get(i0).getEdge().getA() * elements.get(i).getVertex().getX() +
                    elements.get(i0).getEdge().getB() * elements.get(i).getVertex().getY() +
                    elements.get(i0).getEdge().getC();

            if (r == 0) {
                na++;
            } else if (r > 0) {
                iznad++;
            } else {
                ispod++;
            }
        }

        convex = false;
        clockwise = false;

        if (ispod == 0) {
            convex = true;
        } else if (iznad == 0) {
            convex = true;
            clockwise = true;
        }
    }

    public void updateEdges() {
        int i, i0;

        i0 = size - 1;
        for (i = 0; i < size; i++) {
            elements.get(i0).getEdge().setA(elements.get(i0).getVertex().getY() - elements.get(i).getVertex().getY());
            elements.get(i0).getEdge().setB(elements.get(i).getVertex().getX() - elements.get(i0).getVertex().getX());
            elements.get(i0).getEdge().setC(elements.get(i0).getVertex().getX() * elements.get(i).getVertex().getY() -
                    elements.get(i0).getVertex().getY() * elements.get(i).getVertex().getX());


            elements.get(i0).setLeft((elements.get(i0).getVertex().getY() >= elements.get(i).getVertex().getY()) ^ clockwise);

            i0 = i;
        }
    }

    public List<PolygonElement> getElements() {
        return elements;
    }

    public boolean isConvex() {
        return convex;
    }

    public int getSize() {
        return size;
    }

}
