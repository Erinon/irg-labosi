package lab4.model;

public class Edge {

    private int a;
    private int b;
    private int c;

    public Edge() {
    }

    public Edge(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public Edge copy() {
        return new Edge(a, b, c);
    }

}
