package lab4;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import lab4.model.Model;
import lab4.model.Polygon;
import lab4.model.PolygonElement;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;

@SuppressWarnings("Duplicates")
public class PolygonDrawer {

    static {
        GLProfile.initSingleton();
    }

    public static void main(String[] args) {

        SwingUtilities.invokeLater(() -> {
            GLProfile glProfile = GLProfile.getDefault();
            GLCapabilities glCapabilities = new GLCapabilities(glProfile);
            final GLCanvas glCanvas = new GLCanvas(glCapabilities);

            Model model = new Model(640, 480);

            glCanvas.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if (model.isState1()) {
                        model.addVertex();
                    } else {
                        model.testPoint(e.getX(), e.getY());
                    }

                    glCanvas.display();
                }
            });

            glCanvas.addMouseMotionListener(new MouseMotionAdapter() {
                @Override
                public void mouseMoved(MouseEvent e) {
                    if (model.isState1()) {
                        model.updateMouse(e.getX(), e.getY());
                    }

                    glCanvas.display();
                }
            });

            glCanvas.addKeyListener(new KeyAdapter() {
                @Override
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_K) {
                        e.consume();

                        model.toggleKonveksnost();

                        glCanvas.display();
                    } else if (e.getKeyCode() == KeyEvent.VK_P) {
                        e.consume();

                        model.togglePopunjavanje();

                        glCanvas.display();
                    } else if (e.getKeyCode() == KeyEvent.VK_N) {
                        e.consume();

                        model.toggleState();

                        glCanvas.display();
                    }
                }
            });

            glCanvas.addGLEventListener(new GLEventListener() {
                @Override
                public void init(GLAutoDrawable glAutoDrawable) {
                }

                @Override
                public void dispose(GLAutoDrawable glAutoDrawable) {
                }

                @Override
                public void display(GLAutoDrawable glAutoDrawable) {
                    GL2 gl2 = glAutoDrawable.getGL().getGL2();

                    if (model.isKonveksnost()) {
                        gl2.glClearColor(0, 1, 0, 1);
                    } else {
                        gl2.glClearColor(1, 1, 1, 1);
                    }

                    gl2.glClear(GL.GL_COLOR_BUFFER_BIT);

                    gl2.glLoadIdentity();

                    if (model.isPopunjavanje()) {
                        fillPolygon(gl2);
                    } else {
                        drawPolygon(gl2);
                    }

                    gl2.glFlush();
                }

                @Override
                public void reshape(GLAutoDrawable glAutoDrawable, int i, int i1, int i2, int i3) {
                    GL2 gl2 = glAutoDrawable.getGL().getGL2();
                    gl2.glMatrixMode(GL2.GL_PROJECTION);
                    gl2.glLoadIdentity();

                    GLU glu = new GLU();
                    glu.gluOrtho2D(0.0f, i2, i3, 0.0f);

                    gl2.glMatrixMode(GL2.GL_MODELVIEW);
                    gl2.glLoadIdentity();

                    gl2.glViewport(0, 0, i2, i3);

                    model.setWidth(i2);
                    model.setHeight(i3);
                }

                private void drawPolygon(GL2 gl2) {
                    gl2.glBegin(GL2.GL_LINE_LOOP);
                    gl2.glColor3f(0, 0, 0);

                    for (PolygonElement element : model.getPolygon().getElements()) {
                        gl2.glVertex2f(element.getVertex().getX(), element.getVertex().getY());
                    }

                    gl2.glEnd();
                }

                private void fillPolygon(GL2 gl2) {
                    Polygon polygon = model.getPolygon();

                    //polygon.updateEdges();

                    List<PolygonElement> elements = polygon.getElements();

                    int i, i0, y;
                    int xmin, xmax, ymin, ymax;
                    double L, D, x;

                    xmin = xmax = elements.get(0).getVertex().getX();
                    ymin = ymax = elements.get(0).getVertex().getY();

                    for (i = 1; i < polygon.getSize(); i++) {
                        if (xmin > elements.get(i).getVertex().getX()) {
                            xmin = elements.get(i).getVertex().getX();
                        }

                        if (xmax < elements.get(i).getVertex().getX()) {
                            xmax = elements.get(i).getVertex().getX();
                        }

                        if (ymin > elements.get(i).getVertex().getY()) {
                            ymin = elements.get(i).getVertex().getY();
                        }

                        if (ymax < elements.get(i).getVertex().getY()) {
                            ymax = elements.get(i).getVertex().getY();
                        }
                    }

                    gl2.glBegin(GL2.GL_LINES);
                    gl2.glColor3f(0, 0, 0);

                    for (y = ymin; y <= ymax; y++) {
                        L = xmin;
                        D = xmax;
                        i0 = polygon.getSize() - 1;

                        for (i = 0; i < polygon.getSize(); i0=i++) {
                            if (elements.get(i0).getEdge().getA() == 0) {
                                if (elements.get(i0).getVertex().getY() == y) {
                                    if (elements.get(i0).getVertex().getX() < elements.get(i).getVertex().getX()) {
                                        L = elements.get(i0).getVertex().getX();
                                        D = elements.get(i).getVertex().getX();
                                    } else {
                                        L = elements.get(i).getVertex().getX();
                                        D = elements.get(i0).getVertex().getX();
                                    }

                                    break;
                                }
                            } else {
                                x = (-elements.get(i0).getEdge().getB() * y - elements.get(i0).getEdge().getC()) /
                                        (double)elements.get(i0).getEdge().getA();

                                if (elements.get(i0).isLeft()) {
                                    if (L < x) {
                                        L = x;
                                    }
                                } else {
                                    if (D > x) {
                                        D = x;
                                    }
                                }
                            }
                        }

                        gl2.glVertex2f((int)(L + 0.5), y);
                        gl2.glVertex2f((int)(D + 0.5), y);
                    }

                    gl2.glEnd();
                }
            });

            final JFrame jFrame = new JFrame("Primjer prikaza poligona s ispunjavanjem");
            jFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
            jFrame.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    jFrame.dispose();
                    System.exit(0);
                }
            });
            jFrame.getContentPane().add(glCanvas, BorderLayout.CENTER);
            jFrame.setSize(model.getWidth(), model.getHeight());
            jFrame.setVisible(true);
            glCanvas.requestFocusInWindow();
        });

    }

}
