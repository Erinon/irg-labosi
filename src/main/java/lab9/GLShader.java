package lab9;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import lab1.vector.IVector;
import lab1.vector.Vector;
import lab5.ObjectModel;
import lab5.Vertex3D;
import lab6.Drawer.Drawer;
import lab6.Drawer.FrustumDrawer;
import lab6.Model;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

@SuppressWarnings("Duplicates")
public class GLShader {

    static {
        GLProfile.initSingleton();
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Please provide object file name as only argument.");

            System.exit(1);
        }

        if (!args[0].endsWith(".obj")) {
            System.err.println("Please provide only .obj file type.");
        }

        ObjectModel objectModel = new ObjectModel(args[0]);
        objectModel.normalize(false);

        Model model = new Model();
        Drawer drawer = new FrustumDrawer();

        SwingUtilities.invokeLater(() -> {
            GLProfile glProfile = GLProfile.getDefault();
            GLCapabilities glCapabilities = new GLCapabilities(glProfile);
            final GLCanvas glCanvas = new GLCanvas(glCapabilities);
            //glCapabilities.setDoubleBuffered(true);

            glCanvas.addKeyListener(new KeyAdapter() {
                @Override
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_R) {
                        e.consume();

                        model.incrementAngle();

                        glCanvas.display();
                    } else if (e.getKeyCode() == KeyEvent.VK_L) {
                        e.consume();

                        model.decrementAngle();

                        glCanvas.display();
                    } else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                        e.consume();

                        model.resetAngle();

                        glCanvas.display();
                    } else if (e.getKeyCode() == KeyEvent.VK_K) {
                        e.consume();

                        model.setConstant(true);

                        glCanvas.display();
                    } else if (e.getKeyCode() == KeyEvent.VK_G) {
                        e.consume();

                        model.setConstant(false);

                        glCanvas.display();
                    } else if (e.getKeyCode() == KeyEvent.VK_Z) {
                        e.consume();

                        model.toogleZ();

                        glCanvas.display();
                    }
                }
            });

            glCanvas.addGLEventListener(new GLEventListener() {
                @Override
                public void init(GLAutoDrawable glAutoDrawable) {
                }

                @Override
                public void dispose(GLAutoDrawable glAutoDrawable) {
                }

                @Override
                public void display(GLAutoDrawable glAutoDrawable) {
                    GL2 gl2 = glAutoDrawable.getGL().getGL2();
                    GLU glu = new GLU();

                    if (model.isZ()) {
                        gl2.glEnable(GL2.GL_DEPTH_TEST);
                        gl2.glClearColor(0, 1, 0, 0);
                        gl2.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
                    } else {
                        gl2.glDisable(GL2.GL_DEPTH_TEST);
                        gl2.glClearColor(0, 1, 0, 0);
                        gl2.glClear(GL.GL_COLOR_BUFFER_BIT);
                    }

                    gl2.glLoadIdentity();

                    drawer.viewTransform(glu, model, model.getR() * Math.cos(Math.toRadians(model.getAngle())), 4, model.getR() * Math.sin(Math.toRadians(model.getAngle())),
                            0, 0, 0,
                            0, 1, 0);

                    gl2.glEnable(GL2.GL_LIGHTING);
                    gl2.glLightModelfv(GL2.GL_LIGHT_MODEL_AMBIENT, new float[] {0.0f, 0.0f, 0.0f, 1f}, 0);

                    gl2.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, new float[] {4f, 5f, 3f, 1f}, 0);
                    gl2.glLightfv(GL2.GL_LIGHT0, GL2.GL_AMBIENT, new float[] {0.2f, 0.2f, 0.2f, 1f}, 0);
                    gl2.glLightfv(GL2.GL_LIGHT0, GL2.GL_DIFFUSE, new float[] {0.8f, 0.8f, 0f, 1f}, 0);
                    gl2.glLightfv(GL2.GL_LIGHT0, GL2.GL_SPECULAR, new float[] {0f, 0f, 0f, 1f}, 0);

                    gl2.glEnable(GL2.GL_LIGHT0);

                    if (model.isConstant()) {
                        gl2.glShadeModel(GL2.GL_FLAT);
                    } else {
                        gl2.glShadeModel(GL2.GL_SMOOTH);
                    }

                    drawShape(gl2, objectModel);

                    gl2.glFlush();
                    //glAutoDrawable.swapBuffers();
                }

                @Override
                public void reshape(GLAutoDrawable glAutoDrawable, int i, int i1, int i2, int i3) {
                    GL2 gl2 = glAutoDrawable.getGL().getGL2();
                    GLU glu = new GLU();

                    gl2.glMatrixMode(GL2.GL_PROJECTION);
                    gl2.glLoadIdentity();

                    drawer.persProjection(gl2, glu, model, -0.5, 0.5, -0.5, 0.5, 1, 100);

                    gl2.glMatrixMode(GL2.GL_MODELVIEW);
                    gl2.glViewport(0, 0, i2, i3);
                }

                private void drawShape(GL2 gl2, ObjectModel objModel) {
                    List<Vertex3D> v = objModel.getOrderedVertexes();

                    gl2.glColor3f(1, 0, 0);
                    gl2.glPolygonMode(GL2.GL_FRONT, GL2.GL_FILL);
                    gl2.glEnable(GL2.GL_CULL_FACE);
                    gl2.glCullFace(GL2.GL_BACK);

                    gl2.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT, new float[] {1f, 1f, 1f, 1f}, 0);
                    gl2.glMaterialfv(GL2.GL_FRONT, GL2.GL_DIFFUSE, new float[] {1f, 1f, 1f, 1f}, 0);
                    gl2.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, new float[] {0.01f, 0.01f, 0.01f, 1f}, 0);
                    gl2.glMaterialf(GL2.GL_FRONT, GL2.GL_SHININESS, 96f);

                    for (int i = 0, l = v.size(); i < l; i += 3) {
                        gl2.glBegin(GL2.GL_POLYGON);

                        Vertex3D ve1 = v.get(i);
                        Vertex3D ve2 = v.get(i + 1);
                        Vertex3D ve3 = v.get(i + 2);

                        IVector v1  = new Vector(ve1.getX(), ve1.getY(), ve1.getZ(), 1.);
                        IVector v2  = new Vector(ve2.getX(), ve2.getY(), ve2.getZ(), 1.);
                        IVector v3  = new Vector(ve3.getX(), ve3.getY(), ve3.getZ(), 1.);

                        IVector tv1 = v1.toRowMatrix(false).nMultiply(drawer.getTransformationMatrix()).toVector(false).nFromHomogeneous();
                        IVector tv2 = v2.toRowMatrix(false).nMultiply(drawer.getTransformationMatrix()).toVector(false).nFromHomogeneous();
                        IVector tv3 = v3.toRowMatrix(false).nMultiply(drawer.getTransformationMatrix()).toVector(false).nFromHomogeneous();

                        double[] n1;
                        double[] n2;
                        double[] n3;

                        if (model.isConstant()) {
                            //double[] n = ObjectModel.calculatePlaneNormal(ve1, ve2, ve3).toArray();
                            n1 = n2 = n3 = ObjectModel.calculatePlaneNormal(ve1, ve2, ve3).toArray();
                        } else {
                            n1 = ve1.getN().toArray();
                            n2 = ve2.getN().toArray();
                            n3 = ve3.getN().toArray();
                        }

                        gl2.glNormal3f((float)n1[0], (float)n1[1], (float)n1[2]);
                        drawer.drawVertex(gl2, tv1);

                        gl2.glNormal3f((float)n2[0], (float)n2[1], (float)n2[2]);
                        drawer.drawVertex(gl2, tv2);

                        gl2.glNormal3f((float)n3[0], (float)n3[1], (float)n3[2]);
                        drawer.drawVertex(gl2, tv3);

                        gl2.glEnd();
                    }
                }

            });

            final JFrame jFrame = new JFrame("Osvjetljavanje uporabom OpenGL-a");
            jFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
            jFrame.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    jFrame.dispose();
                    System.exit(0);
                }
            });
            jFrame.getContentPane().add(glCanvas, BorderLayout.CENTER);
            jFrame.setSize(640, 480);
            jFrame.setVisible(true);
            glCanvas.requestFocusInWindow();
        });

    }

}
