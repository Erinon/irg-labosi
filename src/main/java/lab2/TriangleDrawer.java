package lab2;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import lab2.model.Model;
import lab2.model.Point;
import lab2.model.Triangle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;

@SuppressWarnings("Duplicates")
public class TriangleDrawer {

    static {
        GLProfile.initSingleton();
    }

    public static void main(String[] args) {

        SwingUtilities.invokeLater(() -> {
            GLProfile glProfile = GLProfile.getDefault();
            GLCapabilities glCapabilities = new GLCapabilities(glProfile);
            final GLCanvas glCanvas = new GLCanvas(glCapabilities);

            Model model = new Model(640, 480);

            glCanvas.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    model.addPoint(e.getX(), e.getY());

                    glCanvas.display();
                }
            });

            glCanvas.addMouseMotionListener(new MouseMotionAdapter() {
                @Override
                public void mouseMoved(MouseEvent e) {
                    model.setMouseX(e.getX());
                    model.setMouseY(e.getY());

                    glCanvas.display();
                }
            });

            glCanvas.addKeyListener(new KeyAdapter() {
                @Override
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_N) {
                        e.consume();

                        model.nextColor();

                        glCanvas.display();
                    } else if (e.getKeyCode() == KeyEvent.VK_P) {
                        e.consume();

                        model.previousColor();

                        glCanvas.display();
                    }
                }
            });

            glCanvas.addGLEventListener(new GLEventListener() {
                @Override
                public void init(GLAutoDrawable glAutoDrawable) {
                }

                @Override
                public void dispose(GLAutoDrawable glAutoDrawable) {
                }

                @Override
                public void display(GLAutoDrawable glAutoDrawable) {
                    GL2 gl2 = glAutoDrawable.getGL().getGL2();

                    gl2.glClearColor(1, 1, 1, 1);
                    gl2.glClear(GL.GL_COLOR_BUFFER_BIT);

                    gl2.glLoadIdentity();

                    for (Triangle t : model.getTriangles()) {
                        drawTriangle(gl2, t);
                    }

                    drawPartTriangle(gl2, model.getPoints(), model.getMouseX(), model.getMouseY(), model.getColor());

                    drawRectangle(gl2, 5, 5, 5, 5, model.getColor());

                    gl2.glFlush();
                }

                @Override
                public void reshape(GLAutoDrawable glAutoDrawable, int i, int i1, int i2, int i3) {
                    GL2 gl2 = glAutoDrawable.getGL().getGL2();
                    gl2.glMatrixMode(GL2.GL_PROJECTION);
                    gl2.glLoadIdentity();

                    GLU glu = new GLU();
                    glu.gluOrtho2D(0.0f, i2, i3, 0.0f);

                    gl2.glMatrixMode(GL2.GL_MODELVIEW);
                    gl2.glLoadIdentity();

                    gl2.glViewport(0, 0, i2, i3);

                    model.setWidth(i2);
                    model.setHeight(i3);
                }

                private void drawTriangle(GL2 gl2, Triangle triangle) {
                    Point a = triangle.getA();
                    Point b = triangle.getB();
                    Point c = triangle.getC();
                    float[] color = triangle.getColor();

                    gl2.glBegin(GL.GL_TRIANGLES);
                    gl2.glColor3f(color[0], color[1], color[2]);
                    gl2.glVertex2f(a.getX(), a.getY());
                    gl2.glVertex2f(b.getX(), b.getY());
                    gl2.glVertex2f(c.getX(), c.getY());
                    gl2.glEnd();
                }

                private void drawPartTriangle(GL2 gl2, List<Point> points, int mX, int mY, float[] color) {

                    if (points.size() == 1) {
                        Point a = points.get(0);

                        gl2.glBegin(GL.GL_LINES);
                        gl2.glColor3f(color[0], color[1], color[2]);
                        gl2.glVertex2f(a.getX(), a.getY());
                        gl2.glVertex2f(mX, mY);
                        gl2.glEnd();
                    } else if (points.size() == 2) {
                        Point a = points.get(0);
                        Point b = points.get(1);

                        Triangle t = new Triangle(a, b, new Point(mX, mY), model.getColor());

                        drawTriangle(gl2, t);
                    }
                }

                private void drawRectangle(GL2 gl2, float x, float y, float width, float height, float[] color) {
                    gl2.glBegin(GL2.GL_QUADS);
                    gl2.glColor3f(color[0], color[1], color[2]);
                    gl2.glVertex2f(x, y);
                    gl2.glVertex2f(x + width, y);
                    gl2.glVertex2f(x + width, y + height);
                    gl2.glVertex2f(x, y + height);
                    gl2.glEnd();
                }
            });

            final JFrame jFrame = new JFrame("Primjer prikaza obojanog trokuta");
            jFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
            jFrame.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    jFrame.dispose();
                    System.exit(0);
                }
            });
            jFrame.getContentPane().add(glCanvas, BorderLayout.CENTER);
            jFrame.setSize(model.getWidth(), model.getHeight());
            jFrame.setVisible(true);
            glCanvas.requestFocusInWindow();
        });

    }

}
