package lab2.model;

import java.util.LinkedList;
import java.util.List;

public class Model {

    private List<Triangle> triangles;
    private List<Point> points;
    private int width;
    private int height;
    private int mouseX;
    private int mouseY;
    private ColorPicker colorPicker;

    public Model(int width, int height) {
        this.triangles = new LinkedList<>();
        this.points = new LinkedList<>();
        this.width = width;
        this.height = height;
        this.colorPicker = new ColorPicker();
    }

    public void addPoint(float x, float y) {
        if (points.size() >= 2) {
            triangles.add(new Triangle(
                    points.get(0),
                    points.get(1),
                    new Point(x, y),
                    this.getColor()
            ));

            points.clear();
        } else {
            points.add(new Point(x, y));
        }
    }

    public void nextColor() {
        colorPicker.nextColor();
    }

    public void previousColor() {
        colorPicker.previousColor();
    }

    public List<Triangle> getTriangles() {
        return triangles;
    }

    public List<Point> getPoints() {
        return points;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public float[] getColor() {
        return colorPicker.getCurrentColor();
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setMouseX(int mouseX) {
        this.mouseX = mouseX;
    }

    public void setMouseY(int mouseY) {
        this.mouseY = mouseY;
    }

    public int getMouseX() {
        return mouseX;
    }

    public int getMouseY() {
        return mouseY;
    }

}
