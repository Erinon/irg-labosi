package lab2.model;

public class Point {

    private float x;
    private float y;

    public Point(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public Point clone() {
        return new Point(x, y);
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

}
