package lab2.model;

public class ColorPicker {

    private float[][] colors;
    private int index;
    private int numberOfColors;

    public ColorPicker() {
        colors = new float[][] {
                {1, 0, 0},
                {0, 1, 0},
                {0, 0, 1},
                {0, 1, 1},
                {1, 1, 0},
                {1, 0, 1}
        };

        this.index = 0;
        this.numberOfColors = colors.length;
    }

    public float[] getCurrentColor() {
        return this.colors[this.index];
    }

    public void nextColor() {
        this.index = (this.index + 1) % this.numberOfColors;
    }

    public void previousColor() {
        this.index = (this.index + this.numberOfColors - 1) % this.numberOfColors;
    }

}
