package lab2.model;

public class Triangle {

    private Point a;
    private Point b;
    private Point c;
    private float[] color;

    public Triangle(Point a, Point b, Point c, float[] color) {
        this.a = a.clone();
        this.b = b.clone();
        this.c = c.clone();
        this.color = color;
    }

    public Triangle(float x1, float y1,
                    float x2, float y2,
                    float x3, float y3,
                    float[] color) {
        this.a = new Point(x1, y1);
        this.b = new Point(x2, y2);
        this.c = new Point(x3, y3);
        this.color = color;
    }

    public Point getA() {
        return a;
    }

    public Point getB() {
        return b;
    }

    public Point getC() {
        return c;
    }

    public float[] getColor() {
        return color;
    }

}
