package lab5;

import lab1.vector.IVector;

public class Face3D {

    private int[] indexes;
    private double a;
    private double b;
    private double c;
    private double d;
    private IVector n;
    private boolean visible;

    public Face3D(int i1, int i2, int i3, double a, double b, double c, double d) {
        this.indexes = new int[] {i1, i2, i3};
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.visible = true;
    }

    public IVector getN() {
        return n;
    }

    public void setN(IVector n) {
        this.n = n;
    }

    public int[] getIndexes() {
        return indexes.clone();
    }

    public Face3D copy() {
        return new Face3D(indexes[0], indexes[1], indexes[2], a, b, c, d);
    }

    public void setIndexes(int[] indexes) {
        this.indexes = indexes;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }

    public double getD() {
        return d;
    }

    public void setA(double a) {
        this.a = a;
    }

    public void setB(double b) {
        this.b = b;
    }

    public void setC(double c) {
        this.c = c;
    }

    public void setD(double d) {
        this.d = d;
    }

    @Override
    public String toString() {
        return "f " + (indexes[0] + 1) + " " + (indexes[1] + 1) + " " + (indexes[2] + 1);
    }

}
