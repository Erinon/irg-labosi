package lab5;

import java.util.Scanner;

public class ModelDemo {

    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Please provide object file name as only argument.");

            System.exit(1);
        }

        if (!args[0].endsWith(".obj")) {
            System.err.println("Please provide only .obj file type.");
        }

        ObjectModel model = new ObjectModel(args[0]);

        ObjectModel normalized = model.copy();
        normalized.normalize(true);

        String[] split;
        double x;
        double y;
        double z;

        Scanner sc = new Scanner(System.in);

        String line = sc.nextLine().trim();

        while (!line.equals("quit")) {
            if (line.equals("normiraj")) {
                System.out.println(normalized.dumpToOBJ());
            } else {
                split = line.split("\\s+");

                if (split.length == 3) {
                    x = Double.parseDouble(split[0]);
                    y = Double.parseDouble(split[1]);
                    z = Double.parseDouble(split[2]);

                    model.printPointStatus(x, y, z);
                } else {
                    System.err.println("Molim unesite koordinate 3D točke odvojene razmacima.");
                }
            }

            line = sc.nextLine().trim();
        }
    }

}
