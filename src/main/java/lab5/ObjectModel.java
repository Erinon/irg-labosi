package lab5;

import lab1.vector.AbstractVector;
import lab1.vector.IVector;
import lab1.vector.Vector;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ObjectModel {

    private String OBJFile;
    private List<Vertex3D> vertexes;
    private List<Face3D> faces;

    public ObjectModel(String OBJFile) {
        this.OBJFile = OBJFile;
        this.vertexes = new LinkedList<>();
        this.faces = new LinkedList<>();

        readOBJFile();
    }

    private ObjectModel(String OBJFile, List<Vertex3D> vertexes, List<Face3D> faces) {
        this.OBJFile = OBJFile;
        this.vertexes = vertexes;
        this.faces = faces;
    }

    private void calculateVertexNormals() {
        Map<Vertex3D, IVector> vals = new HashMap<>();

        Vertex3D v;

        for (Face3D f : faces) {
            int[] inds = f.getIndexes();

            for (int i : inds) {
                v = vertexes.get(i);

                if (vals.containsKey(v)) {
                    vals.put(v, vals.get(v).nAdd(calculatePlaneNormal(f)));
                } else {
                    vals.put(v, calculatePlaneNormal(f));
                }
            }
        }

        for (Vertex3D ve : vals.keySet()) {
            ve.setN(vals.get(ve).nNormalize());
        }
    }

    public void determineFaceVisibilities1(IVector eye) {
        for (Face3D f : faces) {
            f.setVisible(f.getA() * eye.get(0) + f.getB() * eye.get(1) + f.getC() * eye.get(2) + f.getD() > 0);
        }
    }

    public void determineFaceVisibilities2(IVector eye) {
        for (Face3D f : faces) {
            int[] i = f.getIndexes();

            Vertex3D v0 = vertexes.get(i[0]);
            Vertex3D v1 = vertexes.get(i[1]);
            Vertex3D v2 = vertexes.get(i[2]);

            IVector vv0 = new Vector(v0.getX(), v0.getY(), v0.getZ());
            IVector vv1 = new Vector(v1.getX(), v1.getY(), v1.getZ());
            IVector vv2 = new Vector(v2.getX(), v2.getY(), v2.getZ());

            IVector c = vv0.nAdd(vv1).nAdd(vv2).nScalarMultiply(1./3.);

            IVector e = eye.nSub(c);

            f.setVisible(new Vector(f.getA(), f.getB(), f.getC()).scalarProduct(e) > 0);
        }
    }

    public List<Vertex3D> getFaceVertexes(Face3D f) {
        List<Vertex3D> v = new LinkedList<>();

        int[] i = f.getIndexes();

        v.add(vertexes.get(i[0]));
        v.add(vertexes.get(i[1]));
        v.add(vertexes.get(i[2]));

        return v;
    }

    public List<Vertex3D> getOrderedVertexes() {
        List<Vertex3D> orderedVertexes = new LinkedList<>();

        for (Face3D f : faces) {
            for (int i : f.getIndexes()) {
                orderedVertexes.add(vertexes.get(i));
            }
        }

        return orderedVertexes;
    }

    public void printPointStatus(double x, double y, double z) {
        boolean onObject = false;
        double status;

        for (Face3D face : faces) {
            status = face.getA() * x + face.getB() * y + face.getC() * z + face.getD();

            if (status > 0) {
                System.out.println("Točka je izvan tijela.");

                return;
            } else if (status == 0) {
                onObject = true;
            }
        }

        if (onObject) {
            System.out.println("Točka je na obodu tijela.");
        } else {
            System.out.println("Točka je unutar tijela.");
        }
    }

    private void recalculatePlaneStats() {
        int[] inds;

        for (Face3D f : faces) {
            inds = f.getIndexes();

            Vertex3D v0 = vertexes.get(inds[0]);
            Vertex3D v1 = vertexes.get(inds[1]);
            Vertex3D v2 = vertexes.get(inds[2]);

            IVector norm = calculatePlaneNormal(v0, v1, v2);
            double[] n = norm.toArray();

            double d = -n[0] * v0.getX() - n[1] * v0.getY() - n[2] * v0.getZ();

            f.setA(n[0]);
            f.setB(n[1]);
            f.setC(n[2]);
            f.setD(d);
            f.setN(norm);
        }
    }

    public static IVector calculatePlaneNormal(Vertex3D v0, Vertex3D v1, Vertex3D v2) {
        return vertexToVector(v1).nSub(vertexToVector(v0))
                .nVectorProduct(
                        vertexToVector(v2).nSub(vertexToVector(v0))
                ).nNormalize();
    }

    private IVector calculatePlaneNormal(Face3D f) {
        int[] inds = f.getIndexes();

        Vertex3D v0 = vertexes.get(inds[0]);
        Vertex3D v1 = vertexes.get(inds[1]);
        Vertex3D v2 = vertexes.get(inds[2]);

        return calculatePlaneNormal(v0, v1, v2);
    }

    private static IVector vertexToVector(Vertex3D vertex) {
        return new Vector(vertex.getX(), vertex.getY(), vertex.getZ());
    }

    public ObjectModel copy() {
        List<Vertex3D> copiedVertexes = vertexes.stream()
                .map(Vertex3D::copy)
                .collect(Collectors.toList());
        List<Face3D> copiedFaces = faces.stream()
                .map(Face3D::copy)
                .collect(Collectors.toList());

        return new ObjectModel(OBJFile, copiedVertexes, copiedFaces);
    }

    public String dumpToOBJ() {
        StringBuilder sb = new StringBuilder();

        String prefix = "";

        for (Vertex3D vertex : vertexes) {
            sb.append(prefix);
            prefix = "\n";
            sb.append(vertex);
        }

        for (Face3D face : faces) {
            sb.append(prefix);
            prefix = "\n";
            sb.append(face);
        }

        return sb.toString();
    }

    public void normalize(boolean writeToFile) {
        double xmin = vertexes.get(0).getX();
        double xmax = vertexes.get(0).getX();
        double ymin = vertexes.get(0).getY();
        double ymax = vertexes.get(0).getY();
        double zmin = vertexes.get(0).getZ();
        double zmax = vertexes.get(0).getZ();

        for (Vertex3D vertex : vertexes) {
            if (vertex.getX() < xmin) {
                xmin = vertex.getX();
            }

            if (vertex.getX() > xmax) {
                xmax = vertex.getX();
            }

            if (vertex.getY() < ymin) {
                ymin = vertex.getY();
            }

            if (vertex.getY() > ymax) {
                ymax = vertex.getY();
            }

            if (vertex.getZ() < zmin) {
                zmin = vertex.getZ();
            }

            if (vertex.getZ() > zmax) {
                zmax = vertex.getZ();
            }
        }

        double m_x = (xmin + xmax) / 2.;
        double m_y = (ymin + ymax) / 2.;
        double m_z = (zmin + zmax) / 2.;

        double M = Math.max(Math.max(xmax - xmin, ymax - ymin), zmax - zmin);

        for (Vertex3D vertex : vertexes) {
            vertex.setX((vertex.getX() - m_x) * 2. / M);
            vertex.setY((vertex.getY() - m_y) * 2. / M);
            vertex.setZ((vertex.getZ() - m_z) * 2. / M);
        }

        recalculatePlaneStats();

        calculateVertexNormals();

        if (writeToFile) {
            writeOBJFile("objects/" + OBJFile.substring(0, OBJFile.length() - 4) + "_norm.obj");
        }
    }

    private void writeOBJFile(String path) {
        try (Writer bw = new BufferedWriter(
                new OutputStreamWriter(
                        new BufferedOutputStream(
                                new FileOutputStream(path)), StandardCharsets.UTF_8))) {
            bw.write(dumpToOBJ());
        } catch (FileNotFoundException ex) {
            System.err.println(path + "  not found.");
        } catch (IOException ex) {
            System.err.println("Unable to write to " + path + ".");
        }
    }

    private void readOBJFile() {
        String path = "objects/" + OBJFile;

        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        new BufferedInputStream(
                                new FileInputStream(path)), StandardCharsets.UTF_8))) {
            String[] split;
            double[] n;
            double d;
            int i0;
            int i1;
            int i2;
            Vertex3D v0;
            Vertex3D v1;
            Vertex3D v2;

            String line = br.readLine();

            while (line != null) {
                if (line.startsWith("v")) {
                    split = line.trim().split("\\s+");

                    vertexes.add(new Vertex3D(
                            Double.parseDouble(split[1]),
                            Double.parseDouble(split[2]),
                            Double.parseDouble(split[3])
                    ));
                } else if (line.startsWith("f")) {
                    split = line.trim().split("\\s+");

                    i0 = Integer.parseInt(split[1]) - 1;
                    i1 = Integer.parseInt(split[2]) - 1;
                    i2 = Integer.parseInt(split[3]) - 1;

                    v0 = vertexes.get(i0);
                    v1 = vertexes.get(i1);
                    v2 = vertexes.get(i2);

                    n = calculatePlaneNormal(v0, v1, v2).toArray();

                    d = -n[0] * v0.getX() - n[1] * v0.getY() - n[2] * v0.getZ();

                    faces.add(new Face3D(i0, i1, i2, n[0], n[1], n[2], d));
                }

                line = br.readLine();
            }
        } catch (IOException ex) {
            System.err.println("Unable to read " + path + ".");
        }
    }

    public List<Face3D> getFaces() {
        return faces;
    }

}
