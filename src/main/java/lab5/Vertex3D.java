package lab5;

import lab1.vector.AbstractVector;
import lab1.vector.IVector;

import java.util.Objects;

public class Vertex3D {

    private double x;
    private double y;
    private double z;
    private IVector n;

    public Vertex3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public IVector getN() {
        return n;
    }

    public void setN(IVector n) {
        this.n = n;
    }

    public Vertex3D copy() {
        return new Vertex3D(x, y, z);
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    @Override
    public String toString() {
        return "v " + x + " " + y + " " + z;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Vertex3D other = (Vertex3D) o;

        return Double.compare(other.x, this.x) == 0 && Double.compare(other.y, this.y) == 0 && Double.compare(other.z, this.z) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, z);
    }

}
