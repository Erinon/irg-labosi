package lab10;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

@SuppressWarnings("Duplicates")
public class Mandelbrot {

    static {
        GLProfile.initSingleton();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            GLProfile glProfile = GLProfile.getDefault();
            GLCapabilities glCapabilities = new GLCapabilities(glProfile);
            final GLCanvas glCanvas = new GLCanvas(glCapabilities);
            //glCapabilities.setDoubleBuffered(true);

            Model model = new Model();

            glCanvas.addKeyListener(new KeyAdapter() {
                @Override
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_1) {
                        e.consume();

                        model.setSq(true);

                        glCanvas.display();
                    } else if (e.getKeyCode() == KeyEvent.VK_2) {
                        e.consume();

                        model.setSq(false);

                        glCanvas.display();
                    } else if (e.getKeyCode() == KeyEvent.VK_B) {
                        e.consume();

                        model.setColor(false);

                        glCanvas.display();
                    } else if (e.getKeyCode() == KeyEvent.VK_C) {
                        e.consume();

                        model.setColor(true);

                        glCanvas.display();
                    } else if (e.getKeyCode() == KeyEvent.VK_X) {
                        e.consume();

                        model.zoomOut();

                        glCanvas.display();
                    } else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                        e.consume();

                        model.reset();

                        glCanvas.display();
                    }
                }
            });

            glCanvas.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if (e.getButton() == MouseEvent.BUTTON1) {
                        model.zoomIn(e.getX(), e.getY());
                    }

                    glCanvas.display();
                }
            });

            glCanvas.addGLEventListener(new GLEventListener() {
                @Override
                public void init(GLAutoDrawable glAutoDrawable) {
                }

                @Override
                public void dispose(GLAutoDrawable glAutoDrawable) {
                }

                @Override
                public void display(GLAutoDrawable glAutoDrawable) {
                    GL2 gl2 = glAutoDrawable.getGL().getGL2();

                    gl2.glClearColor(1, 1, 1, 1);
                    gl2.glClear(GL.GL_COLOR_BUFFER_BIT);
                    gl2.glLoadIdentity();

                    drawMandelbrot(gl2);

                    gl2.glFlush();
                    //glAutoDrawable.swapBuffers();
                }

                @Override
                public void reshape(GLAutoDrawable glAutoDrawable, int i, int i1, int i2, int i3) {
                    GL2 gl2 = glAutoDrawable.getGL().getGL2();
                    GLU glu = new GLU();

                    gl2.glMatrixMode(GL2.GL_PROJECTION);
                    gl2.glLoadIdentity();
                    //glu.gluOrtho2D(i2 / 2., i2 / 2., i3 / 2., i3 / 2.);
                    glu.gluOrtho2D(0, i2, i3, 0);

                    gl2.glMatrixMode(GL2.GL_MODELVIEW);
                    gl2.glLoadIdentity();
                    gl2.glViewport(0, 0, i2, i3);

                    model.setWidth(i2);
                    model.setHeight(i3);
                }

                private void drawMandelbrot(GL2 gl2) {
                    Complex c;
                    int n;

                    gl2.glBegin(GL.GL_POINTS);

                    for (int y = 0; y <= model.getHeight(); y++) {
                        for (int x = 0; x <= model.getWidth(); x++) {
                            c = new Complex(model.getU(x), model.getV(y));

                            if (model.isSq()) {
                                n = divergenceTest(c);
                            } else {
                                n = divergenceTest2(c);
                            }

                            if (model.isColor()) {
                                colorScheme2(gl2, n);
                            } else {
                                colorScheme1(gl2, n);
                            }

                            gl2.glVertex2i(x, y);
                        }
                    }

                    gl2.glEnd();
                }

                private int divergenceTest(Complex c) {
                    Complex z = new Complex(0., 0.);

                    for (int i = 1; i <= model.getMaxLimit(); i++) {
                        z.square().add(c);

                        if (z.squareModule() > 4) {
                            return i;
                        }
                    }

                    return -1;
                }

                private int divergenceTest2(Complex c) {
                    Complex z = new Complex(0., 0.);

                    for (int i = 1; i <= model.getMaxLimit(); i++) {
                        z.cube().add(c);

                        if (z.squareModule() > 4) {
                            return i;
                        }
                    }

                    return -1;
                }

                private void colorScheme1(GL2 gl2, int n) {
                    if (n == -1) {
                        gl2.glColor3f(0f, 0f, 0f);
                    } else {
                        gl2.glColor3f(1f, 1f, 1f);
                    }
                }

                private void colorScheme2(GL2 gl2, int n) {
                    int maxLimit = model.getMaxLimit();

                    if (n == -1) {
                        gl2.glColor3f(0f, 0f, 0f);
                    } else if (maxLimit < 16) {
                        int r = (int)((n - 1) / (double)(maxLimit - 1) * 255 + 0.5);
                        int g = 255 - r;
                        int b = ((n - 1) % (maxLimit / 2)) * 255 / (maxLimit / 2);

                        gl2.glColor3f((float)r / 255f, (float)g / 255f, (float)b / 255f);
                    } else {
                        int lim = maxLimit < 32 ? maxLimit : 32;
                        int r = (n - 1) * 255 / lim;
                        int g = ((n - 1) % (lim / 4)) * 255 / (lim / 4);
                        int b = ((n - 1) % (lim / 8)) * 255 / (lim / 8);

                        gl2.glColor3f((float)r / 255f, (float)g / 255f, (float)b / 255f);
                    }
                }
            });

            final JFrame jFrame = new JFrame("Mandelbrotov fraktal");
            jFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
            jFrame.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    jFrame.dispose();
                    System.exit(0);
                }
            });
            jFrame.getContentPane().add(glCanvas, BorderLayout.CENTER);
            jFrame.setSize(800, 800 + 37);
            jFrame.setVisible(true);
            glCanvas.requestFocusInWindow();
        });

    }

}
