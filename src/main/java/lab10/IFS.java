package lab10;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.Random;

@SuppressWarnings("Duplicates")
public class IFS {

    static {
        GLProfile.initSingleton();
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Please provide IFS file as only argument.");

            System.exit(1);
        }

        if (!args[0].endsWith(".txt")) {
            System.err.println("Please provide only .txt file type.");
        }

        SwingUtilities.invokeLater(() -> {
            GLProfile glProfile = GLProfile.getDefault();
            GLCapabilities glCapabilities = new GLCapabilities(glProfile);
            final GLCanvas glCanvas = new GLCanvas(glCapabilities);
            //glCapabilities.setDoubleBuffered(true);

            Model model = new Model();
            model.readIFSModel(args[0]);

            glCanvas.addGLEventListener(new GLEventListener() {
                @Override
                public void init(GLAutoDrawable glAutoDrawable) {
                }

                @Override
                public void dispose(GLAutoDrawable glAutoDrawable) {
                }

                @Override
                public void display(GLAutoDrawable glAutoDrawable) {
                    GL2 gl2 = glAutoDrawable.getGL().getGL2();

                    gl2.glClearColor(1, 1, 1, 1);
                    gl2.glClear(GL.GL_COLOR_BUFFER_BIT);
                    gl2.glLoadIdentity();

                    drawIFS(gl2);

                    gl2.glFlush();
                    //glAutoDrawable.swapBuffers();
                }

                @Override
                public void reshape(GLAutoDrawable glAutoDrawable, int i, int i1, int i2, int i3) {
                    GL2 gl2 = glAutoDrawable.getGL().getGL2();
                    GLU glu = new GLU();

                    gl2.glMatrixMode(GL2.GL_PROJECTION);
                    gl2.glLoadIdentity();
                    //glu.gluOrtho2D(i2 / 2., i2 / 2., i3 / 2., i3 / 2.);
                    glu.gluOrtho2D(0, i2, 0, i3);

                    gl2.glMatrixMode(GL2.GL_MODELVIEW);
                    gl2.glLoadIdentity();
                    gl2.glViewport(0, 0, i2, i3);

                    model.setWidth(i2);
                    model.setHeight(i3);
                }

                private int zaokruzi(double d) {
                    if (d >= 0) {
                        return (int)(d + 0.5);
                    } else {
                        return (int)(d - 0.5);
                    }
                }

                private void drawIFS(GL2 gl2) {
                    Random r = new Random();
                    double x0, y0;
                    List<double[]> table = model.getTable();

                    gl2.glColor3f(0.0f, 0.7f, 0.3f);
                    gl2.glBegin(GL.GL_POINTS);

                    for (int brojac = 0; brojac < model.getPointsNumber(); brojac++) {
                        x0 = 0.;
                        y0 = 0.;

                        for (int iter = 0; iter < model.getLimit(); iter++) {
                            double x = 0, y = 0;
                            double p = r.nextDouble();
                            double sum = 0.;

                            for (double[] t : table) {
                                sum += t[6];

                                if (p < sum) {
                                    x = t[0] * x0 + t[1] * y0 + t[4];
                                    y = t[2] * x0 + t[3] * y0 + t[5];

                                    break;
                                }
                            }

                            x0 = x;
                            y0 = y;
                        }

                        gl2.glVertex2i(zaokruzi(x0 * model.getEta1() + model.getEta2()), zaokruzi(y0 * model.getEta3() + model.getEta4()));
                    }

                    gl2.glEnd();
                }
            });

            final JFrame jFrame = new JFrame("IFS fraktal");
            jFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
            jFrame.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    jFrame.dispose();
                    System.exit(0);
                }
            });
            jFrame.getContentPane().add(glCanvas, BorderLayout.CENTER);
            jFrame.setSize(600, 600 + 37);
            jFrame.setVisible(true);
            glCanvas.requestFocusInWindow();
        });

    }

}
