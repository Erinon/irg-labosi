package lab10;

public class Complex {

    private double re;
    private double im;

    public Complex(double re, double im) {
        this.re = re;
        this.im = im;
    }

    public Complex clone() {
        return new Complex(re, im);
    }

    public Complex nAdd(Complex c) {
        return clone().add(c);
    }

    public Complex add(Complex c) {
        re += c.re;
        im += c.im;

        return this;
    }

    public Complex nSub(Complex c) {
        return clone().sub(c);
    }

    public Complex sub(Complex c) {
        re -= c.re;
        im -= c.im;

        return this;
    }

    public Complex nMultiply(Complex c) {
        return clone().multiply(c);
    }

    public Complex multiply(Complex c) {
        double nre = re * c.re - im * c.im;
        double nim = re * c.im + c.re * im;

        re = nre;
        im = nim;

        return this;
    }

    public Complex square() {
        return multiply(this);
    }

    public Complex nSquare() {
        return clone().square();
    }

    public Complex cube() {
        Complex temp = clone();

        return square().multiply(temp);
    }

    public Complex nCube() {
        return clone().cube();
    }

    public double squareModule() {
        return re * re + im * im;
    }

    public double module() {
        return Math.sqrt(squareModule());
    }

    public double getRe() {
        return re;
    }

    public double getIm() {
        return im;
    }

    public boolean equals(Complex c) {
        return re == c.re && im == c.im;
    }

    @Override
    public String toString() {
        return re + " + " + im + "i";
    }

}
