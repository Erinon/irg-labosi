package lab10;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class Model {

    private int width;
    private int height;
    private double umin;
    private double umax;
    private double vmin;
    private double vmax;
    private int maxLimit;
    private boolean sq;
    private boolean color;
    private Stack<Double> states;
    private double scale;
    private int pointsNumber;
    private int limit;
    private double eta1;
    private double eta2;
    private double eta3;
    private double eta4;
    private List<double[]> table;

    public Model() {
        this.umin = -2.;
        this.umax = 1.;
        this.vmin = -1.2;
        this.vmax = 1.2;
        this.maxLimit = 1000;
        this.sq = true;
        this.color = false;
        this.states = new Stack<>();
        this.scale = 16.;
    }

    public void zoomIn(int x, int y) {
        states.push(vmax);
        states.push(vmin);
        states.push(umax);
        states.push(umin);

        double u = getU(x);
        double v = getV(y);

        umin = u - (umax - umin) / scale / 2.;
        umax = u + (umax - umin) / scale / 2.;
        vmin = v - (vmax - vmin) / scale / 2.;
        vmax = v + (vmax - vmin) / scale / 2.;
    }

    public void zoomOut() {
        if (states.empty()) {
            return;
        }

        umin = states.pop();
        umax = states.pop();
        vmin = states.pop();
        vmax = states.pop();
    }

    public void reset() {
        while (!states.empty()) {
            zoomOut();
        }
    }

    public void setColor(boolean color) {
        this.color = color;
    }

    public boolean isColor() {
        return color;
    }

    public void setSq(boolean sq) {
        this.sq = sq;
    }

    public boolean isSq() {
        return sq;
    }

    public double getU(int x) {
        return x * (umax - umin) / (width - 1) + umin;
    }

    public double getV(int y) {
        return y * (vmax - vmin) / (height - 1) + vmin;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getMaxLimit() {
        return maxLimit;
    }

    public int getPointsNumber() {
        return pointsNumber;
    }

    public int getLimit() {
        return limit;
    }

    public double getEta1() {
        return eta1;
    }

    public double getEta2() {
        return eta2;
    }

    public double getEta3() {
        return eta3;
    }

    public double getEta4() {
        return eta4;
    }

    public List<double[]> getTable() {
        return table;
    }

    public void readIFSModel(String file) {
        String path = "IFS/" + file;
        System.out.println("Reading file " + path + ".");

        this.table = new LinkedList<>();

        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        new BufferedInputStream(
                                new FileInputStream(path)), StandardCharsets.UTF_8))) {

            String line;
            String[] spl;
            int i;

            line = br.readLine().trim();
            while (line.startsWith("#")) {
                line = br.readLine().trim();
            }

            spl = line.split("\\s+");
            this.pointsNumber = Integer.parseInt(spl[0]);

            line = br.readLine().trim();
            while (line.startsWith("#")) {
                line = br.readLine().trim();
            }

            spl = line.split("\\s+");
            this.limit = Integer.parseInt(spl[0]);

            line = br.readLine().trim();
            while (line.startsWith("#")) {
                line = br.readLine().trim();
            }

            spl = line.split("\\s+");
            this.eta1 = Double.parseDouble(spl[0]);
            this.eta2 = Double.parseDouble(spl[1]);

            line = br.readLine().trim();
            while (line.startsWith("#")) {
                line = br.readLine().trim();
            }

            spl = line.split("\\s+");
            this.eta3 = Double.parseDouble(spl[0]);
            this.eta4 = Double.parseDouble(spl[1]);

            line = br.readLine();
            while (line != null) {
                line = line.trim();

                if (line.startsWith("#")) {
                    line = br.readLine();

                    continue;
                }

                spl = line.split("\\s+");

                double[] trans = new double[7];

                for (i = 0; i < 7; i++) {
                    trans[i] = Double.parseDouble(spl[i]);
                }

                table.add(trans);

                line = br.readLine();
            }

            System.out.println("Reading of file " + path + " done.");
        } catch (IOException ex) {
            System.err.println("Unable to read " + path + ".");
        }
    }

}
