package lab1.helpers;

import lab1.matrix.AbstractMatrix;
import lab1.matrix.IMatrix;
import lab1.matrix.Matrix;
import lab1.vector.IVector;

public class MatrixVectorView extends AbstractMatrix {

    private IVector vector;
    private boolean asRowMatrix;

    public MatrixVectorView(IVector vector, boolean asRowMatrix) {
        this.vector = vector;
        this.asRowMatrix = asRowMatrix;
    }

    @Override
    public int getRowsCount() {
        if (asRowMatrix) {
            return 1;
        } else {
            return vector.getDimension();
        }
    }

    @Override
    public int getColsCount() {
        if (asRowMatrix) {
            return vector.getDimension();
        } else {
            return 1;
        }
    }

    @Override
    public double get(int i, int j) {
        checkIndexes(i, j);

        if (asRowMatrix) {
            return vector.get(j);
        } else {
            return vector.get(i);
        }
    }

    @Override
    public IMatrix set(int i, int j, double x) {
        checkIndexes(i, j);

        if (asRowMatrix) {
            vector.set(j, x);
        } else {
            vector.set(i, x);
        }

        return this;
    }

    @Override
    public IMatrix copy() {
        return new MatrixVectorView(vector, asRowMatrix);
    }

    @Override
    public IMatrix newInstance(int rows, int cols) {
        return new Matrix(rows, cols);
    }

    private void checkIndexes(int i, int j) {
        if (i >= this.getRowsCount() || i < 0 || j >= this.getColsCount() || j < 0) {
            throw new IndexOutOfBoundsException();
        }
    }

}
