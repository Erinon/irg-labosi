package lab1.helpers;

import lab1.matrix.IMatrix;
import lab1.vector.AbstractVector;
import lab1.vector.IVector;
import lab1.vector.Vector;

public class VectorMatrixView extends AbstractVector {

    private IMatrix matrix;
    private int dimension;
    private boolean rowMatrix;

    public VectorMatrixView(IMatrix matrix) {
        if (matrix.getRowsCount() == 1) {
            dimension = matrix.getColsCount();
            rowMatrix = true;
        } else if (matrix.getColsCount() == 1) {
            dimension = matrix.getRowsCount();
            rowMatrix = false;
        } else {
            throw new IllegalArgumentException();
        }

        this.matrix = matrix;
    }

    @Override
    public double get(int index) {
        checkIndex(index);

        if (rowMatrix) {
            return matrix.get(0, index);
        } else {
            return matrix.get(index, 0);
        }
    }

    @Override
    public IVector set(int index, double value) {
        checkIndex(index);

        if (rowMatrix) {
            matrix.set(0, index, value);
        } else {
            matrix.set(index, 0, value);
        }

        return this;
    }

    @Override
    public int getDimension() {
        return this.dimension;
    }

    @Override
    public IVector copy() {
        return new VectorMatrixView(this.matrix);
    }

    @Override
    public IVector newInstance(int n) {
        return matrix.newInstance(1, n).toVector(false);
    }

    private void checkIndex(int index) {
        if (index >= this.dimension || index < 0) {
            throw new IndexOutOfBoundsException();
        }
    }

}
