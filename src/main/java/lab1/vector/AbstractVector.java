package lab1.vector;

import lab1.helpers.MatrixVectorView;
import lab1.matrix.IMatrix;

public abstract class AbstractVector implements IVector {

    public AbstractVector() {
    }

    @Override
    public IVector copyPart(int n) {
        if (n < 0) {
            throw new IllegalArgumentException();
        }

        IVector vector = this.newInstance(n);

        for (int i = 0, d = this.getDimension(); i < d && i < n; i++) {
            vector.set(i, this.get(i));
        }

        return vector;
    }

    @Override
    public IVector add(IVector other) {
        if (this.getDimension() != other.getDimension()) {
            throw new IllegalArgumentException();
        }

        for (int i = this.getDimension() - 1; i >= 0; i--) {
            this.set(i, this.get(i) + other.get(i));
        }

        return this;
    }

    @Override
    public IVector nAdd(IVector other) {
        return this.copy().add(other);
    }

    @Override
    public IVector sub(IVector other) {
        return this.add(other.nScalarMultiply(-1));
    }

    @Override
    public IVector nSub(IVector other) {
        return this.copy().sub(other);
    }

    @Override
    public IVector scalarMultiply(double x) {
        for (int i = this.getDimension() - 1; i >= 0; i--) {
            this.set(i, this.get(i) * x);
        }

        return this;
    }

    @Override
    public IVector nScalarMultiply(double x) {
        return this.copy().scalarMultiply(x);
    }

    @Override
    public double norm() {
        double norm = 0.;

        for (int i = this.getDimension() - 1; i >= 0; i--) {
            norm += this.get(i) * this.get(i);
        }

        return Math.sqrt(norm);
    }

    @Override
    public IVector normalize() {
        double normRec = 1. / this.norm();

        return this.scalarMultiply(normRec);
    }

    @Override
    public IVector nNormalize() {
        return this.copy().normalize();
    }

    @Override
    public double cosine(IVector other) {
        if (this.getDimension() == 0 || other.getDimension() == 0) {
            throw new IllegalArgumentException();
        }

        return this.scalarProduct(other) / (this.norm() * other.norm());
    }

    @Override
    public double scalarProduct(IVector other) {
        if (this.getDimension() != other.getDimension()) {
            throw new IllegalArgumentException();
        }

        double product = 0.;

        for (int i = this.getDimension() - 1; i >= 0; i--) {
            product += this.get(i) * other.get(i);
        }

        return product;
    }

    @Override
    public IVector nVectorProduct(IVector other) {
        if (this.getDimension() != 3 || other.getDimension() != 3) {
            throw new IllegalArgumentException();
        }

        return new Vector(
                this.get(1) * other.get(2) - this.get(2) * other.get(1),
                this.get(2) * other.get(0) - this.get(0) * other.get(2),
                this.get(0) * other.get(1) - this.get(1) * other.get(0)
        );
    }

    @Override
    public IVector nFromHomogeneous() {
        return this.copyPart(this.getDimension() - 1)
                .scalarMultiply(1. / this.get(this.getDimension() - 1));
    }

    @Override
    public IMatrix toRowMatrix(boolean liveView) {
        if (liveView) {
            return new MatrixVectorView(this, true);
        } else {
            return new MatrixVectorView(this.copy(), true);
        }
    }

    @Override
    public IMatrix toColumnMatrix(boolean liveView) {
        if (liveView) {
            return new MatrixVectorView(this, false);
        } else {
            return new MatrixVectorView(this.copy(), false);
        }
    }

    @Override
    public double[] toArray() {
        double[] vector = new double[this.getDimension()];

        for (int i = this.getDimension() - 1; i >= 0; i--) {
            vector[i] = this.get(i);
        }

        return vector;
    }

    public String toString(int precision) {
        if (precision < 0) {
            throw new IllegalArgumentException();
        }

        if (this.getDimension() == 0) {
            return "[ ]";
        }

        String format = "%." + precision + "f";
        StringBuilder sb = new StringBuilder();

        sb.append('[');

        sb.append(String.format(format, this.get(0)));

        for (int i = 1, d = this.getDimension(); i < d; i++) {
            sb.append(", ");
            sb.append(String.format(format, this.get(i)));
        }

        sb.append(']');

        return sb.toString();
    }

    public String toString() {
        return toString(3);
    }

}
