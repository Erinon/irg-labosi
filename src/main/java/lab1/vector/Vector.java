package lab1.vector;

import java.util.Arrays;

public class Vector extends AbstractVector {

    private double[] elements;
    private int dimension;
    private boolean readOnly;

    public Vector(double ...elements) {
        this.elements = elements;
        this.dimension = elements.length;
        this.readOnly = false;
    }

    public Vector(boolean readOnly, boolean elementsOwner, double[] elements) {
        if (elementsOwner) {
            this.elements = elements;
        } else {
            this.elements = elements.clone();
        }

        this.dimension = elements.length;
        this.readOnly = readOnly;
    }

    @Override
    public double get(int index) {
        checkIndex(index);

        return this.elements[index];
    }

    @Override
    public IVector set(int index, double value) {
        if (this.readOnly) {
            throw new IllegalAccessError();
        }

        checkIndex(index);

        this.elements[index] = value;

        return this;
    }

    @Override
    public int getDimension() {
        return dimension;
    }

    @Override
    public IVector copy() {
        return new Vector(this.readOnly, false, this.elements);
    }

    @Override
    public IVector newInstance(int n) {
        if (n < 0) {
            throw new IllegalArgumentException();
        }

        return new Vector(new double[n]);
    }

    public static Vector parseSimple(String vector) {
        if (vector.trim().isEmpty()) {
            return new Vector();
        }

        return new Vector(
                Arrays.stream(vector.trim().split("\\s+"))
                .mapToDouble(Double::parseDouble)
                .toArray()
        );
    }

    private void checkIndex(int index) {
        if (index >= dimension || index < 0) {
            throw new IndexOutOfBoundsException();
        }
    }

}
