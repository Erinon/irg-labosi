package lab1;

import lab1.matrix.IMatrix;
import lab1.matrix.Matrix;
import lab1.vector.IVector;

public class IRG {

    public static IMatrix translate3D(float dx, float dy, float dz) {
        return new Matrix(new double[][]{
                {1., 0., 0., 0.},
                {0., 1., 0., 0.},
                {0., 0., 1., 0.},
                {dx, dy, dz, 1.}
                }, true);
    }

    public static IMatrix scale3D(float sx, float sy, float sz) {
        return new Matrix(new double[][]{
                {sx, 0., 0., 0.},
                {0., sy, 0., 0.},
                {0., 0., sz, 0.},
                {0., 0., 0., 1.}
        }, true);
    }

    public static IMatrix lookAtMatrix(IVector eye, IVector center, IVector viewUp) {
        IVector h = center.nSub(eye);
        h.normalize();

        IVector u = h.nVectorProduct(viewUp);
        u.normalize();

        IVector v = u.nVectorProduct(h);
        v.normalize();

        IMatrix M2 = new Matrix(new double[][] {
                {u.get(0), v.get(0), -h.get(0), 0.},
                {u.get(1), v.get(1), -h.get(1), 0.},
                {u.get(2), v.get(2), -h.get(2), 0.},
                {0., 0., 0., 1.}
        }, true);

        return translate3D(-(float)eye.get(0), -(float)eye.get(1), -(float)eye.get(2)).nMultiply(M2);
    }

    public static IMatrix buildFrustumMatrix(double l, double r, double b, double t, double n, double f) {
        return new Matrix(new double[][]{
                {2*n/(r-l), 0., 0., 0.},
                {0., 2*n/(t-b), 0., 0.},
                {(r+l)/(r-l), (t+b)/(t-b), -(f+n)/(f-n), -1.},
                {0., 0., -2*f*n/(f-n), 0.}
        }, true);
    }

    public static boolean isAntiClockwise(IVector v0, IVector v1, IVector v2) {
        return (v1.get(0) - v0.get(0)) * (v2.get(1) - v0.get(1)) - (v2.get(0) - v0.get(0)) * (v1.get(1) - v0.get(1)) > 0;
    }

}
