package lab1;

import lab1.matrix.IMatrix;
import lab1.matrix.Matrix;
import lab1.vector.IVector;
import lab1.vector.Vector;

import java.util.Scanner;

public class LibraryDemo {

    public static void main(String[] args) {
        //examples();

        //demonstration();
    }

    private static void demonstration() {
        ex1();

        ex2();

        ex3();
    }

    private static void examples() {
        baricentricneKoordinate1();

        System.out.println();

        //sustav1();

        System.out.println();

        baricentricneKoordinate2();

        System.out.println();

        //reflectedVector();

        System.out.println();
    }

    private static void ex1() {
        IVector hv1 = Vector.parseSimple("2 3 -4");
        IVector hv2 = Vector.parseSimple("-1 4 -3");

        IVector v1 = hv1.nAdd(hv2);
        System.out.println("v1: ");
        System.out.println(v1);
        System.out.println();

        double s = v1.scalarProduct(hv2);
        System.out.println("s: ");
        System.out.println(s);
        System.out.println();

        IVector hv3 = Vector.parseSimple("2 2 4");

        IVector v2 = v1.nVectorProduct(hv3);
        System.out.println("v2: ");
        System.out.println(v2);
        System.out.println();

        IVector v3 = v2.nNormalize();
        System.out.println("v3: ");
        System.out.println(v3);
        System.out.println();

        IVector v4 = v2.scalarMultiply(-1);
        System.out.println("v4: ");
        System.out.println(v4);
        System.out.println();

        IMatrix hm1 = Matrix.parseSimple("1 2 3 | 2 1 3 | 4 5 1");
        IMatrix hm2 = Matrix.parseSimple("-1 2 -3 | 5 -2 7 | -4 -1 3");

        IMatrix m1 = hm1.nAdd(hm2);
        System.out.println("m1: ");
        System.out.println(m1);
        System.out.println();

        IMatrix m2 = hm1.nMultiply(hm2.nTranspose(false));
        System.out.println("m2: ");
        System.out.println(m2);
        System.out.println();

        IMatrix hm3 = Matrix.parseSimple("-24 18 5 | 20 -15 -4 | -5 4 1");
        IMatrix hm4 = Matrix.parseSimple("1 2 3 | 0 1 4 | 5 6 0");

        IMatrix m3 = hm3.nInvert().nMultiply(hm4.nInvert());
        System.out.println("m3: ");
        System.out.println(m3);
        System.out.println();
    }

    private static void ex2() {
        Scanner s = new Scanner(System.in);

        double[][] left = new double[3][3];
        double[][] right = new double[3][1];

        System.out.println("Unesite podatke o 3 jednadžbe sa 3 nepoznanice: ");

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                left[i][j] = s.nextDouble();
            }

            right[i][0] = s.nextDouble();
        }

        IMatrix m1 = new Matrix(left, false);
        IMatrix m2 = new Matrix(right, false);

        IVector result = m1.nInvert().nMultiply(m2).toVector(false);

        System.out.println("[x, y, z] = " + result);
        System.out.println();
    }

    private static void ex3() {
        Scanner s = new Scanner(System.in);

        double[][] left = new double[3][3];
        double[][] right = new double[3][1];

        System.out.println("Unesite vrhove trokuta i točku: ");

        for (int j = 0; j < 3; j++) {
            for (int i = 0; i < 3; i++) {
                left[i][j] = s.nextDouble();
            }
        }

        for (int i = 0; i < 3; i++) {
            right[i][0] = s.nextDouble();
        }

        IMatrix m1 = new Matrix(left, false);
        IMatrix m2 = new Matrix(right, false);

        IVector result = m1.nInvert().nMultiply(m2).toVector(false);

        System.out.println("Baricentricne koordinate su: " + result);
        System.out.println();
    }

    private static void baricentricneKoordinate1() {
        IVector a = Vector.parseSimple("1 0 0");
        IVector b = Vector.parseSimple("5 0 1");
        IVector c = Vector.parseSimple("3 8 0");

        IVector t = Vector.parseSimple("1 5 3");

        double pov = b.nSub(a).nVectorProduct(c.nSub(a)).norm() / 2.;

        double povA = b.nSub(t).nVectorProduct(c.nSub(t)).norm() / 2.;
        double povB = a.nSub(t).nVectorProduct(c.nSub(t)).norm() / 2.;
        double povC = a.nSub(t).nVectorProduct(b.nSub(t)).norm() / 2.;

        double t1 = povA / pov;
        double t2 = povB / pov;
        double t3 = povC / pov;

        System.out.println("Baricentricne koordinate su: ( " + t1 + ", " + t2 + ", " + t3 + " ).");
    }

    private static void sustav1() {
        IMatrix a = Matrix.parseSimple("3 5 | 2 10");
        IMatrix r = Matrix.parseSimple("2 | 8");

        IMatrix v = a.nInvert().nMultiply(r);

        System.out.println("Rjesenje sustava je: ");
        System.out.println(v);
    }

    private static void baricentricneKoordinate2() {
        IMatrix a = Matrix.parseSimple("1 5 3 | 0 0 8 | 1 0 0");
        IMatrix r = Matrix.parseSimple("1 | 0 | 1");

        IMatrix v = a.nInvert().nMultiply(r);

        System.out.println("Baricentricne koordinate su: ");
        System.out.println(v);
    }

    private static void reflectedVector() {
        Scanner s = new Scanner(System.in);

        System.out.println("Unesite dimenzije vektora: ");

        int d = s.nextInt();

        if (d < 1) {
            throw new IllegalArgumentException();
        }

        double[] nArr = new double[d];
        double[] mArr = new double[d];

        System.out.println("Unesite vektore n i m: ");

        for (int i = 0; i < d; i++) {
            nArr[i] = s.nextDouble();
        }

        for (int i = 0; i < d; i++) {
            mArr[i] = s.nextDouble();
        }

        IVector n = new Vector(nArr);
        IVector m = new Vector(mArr);

        IVector result = n.nNormalize().nScalarMultiply(2).nScalarMultiply(m.scalarProduct(n.nNormalize())).nSub(m);

        System.out.println("Reflektirani vektor je: ");
        System.out.println(result);
        System.out.println();
    }

}
