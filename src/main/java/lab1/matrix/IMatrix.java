package lab1.matrix;

import lab1.vector.IVector;

public interface IMatrix {

    int getRowsCount();

    int getColsCount();

    double get(int i, int j);

    IMatrix set(int i, int j, double x);

    IMatrix copy();

    IMatrix newInstance(int rows, int cols);

    IMatrix nTranspose(boolean liveView);

    IMatrix add(IMatrix other);

    IMatrix nAdd(IMatrix other);

    IMatrix sub(IMatrix other);

    IMatrix nSub(IMatrix other);

    IMatrix nMultiply(IMatrix other);

    double determinant();

    IMatrix subMatrix(int row, int col, boolean liveView);

    IMatrix nInvert();

    double[][] toArray();

    IVector toVector(boolean liveView);

}
