package lab1.matrix;

public class MatrixSubMatrixView extends AbstractMatrix implements IMatrix {

    private IMatrix matrix;
    private int[] rowIndexes;
    private int[] colIndexes;

    public MatrixSubMatrixView(IMatrix matrix, int row, int col) {
        this.matrix = matrix;

        int r = matrix.getRowsCount();
        int c = matrix.getColsCount();

        if (row >= r || row < 0 || col >= c || col < 0) {
            throw new IllegalArgumentException();
        }

        rowIndexes = new int[r - 1];

        int br = 0;

        for (int i = 0; i < r; i++) {
            if (i != row) {
                rowIndexes[br++] = i;
            }
        }

        colIndexes = new int[c - 1];

        br = 0;

        for (int i = 0; i < c; i++) {
            if (i != col) {
                colIndexes[br++] = i;
            }
        }
    }

    private MatrixSubMatrixView(IMatrix matrix, int[] rows, int[] cols) {
        this.matrix = matrix;
        this.rowIndexes = rows.clone();
        this.colIndexes = cols.clone();
    }

    @Override
    public int getRowsCount() {
        return rowIndexes.length;
    }

    @Override
    public int getColsCount() {
        return colIndexes.length;
    }

    @Override
    public double get(int i, int j) {
        checkIndexes(i, j);

        return matrix.get(this.rowIndexes[i], this.colIndexes[j]);
    }

    @Override
    public IMatrix set(int i, int j, double x) {
        checkIndexes(i, j);

        return matrix.set(this.rowIndexes[i], this.colIndexes[j], x);
    }

    @Override
    public IMatrix copy() {
        return new MatrixSubMatrixView(matrix.copy(), rowIndexes.clone(), colIndexes.clone());
    }

    @Override
    public IMatrix newInstance(int rows, int cols) {
        return this.matrix.newInstance(rows, cols);
    }

    private void checkIndexes(int i, int j) {
        if (i >= this.getRowsCount() || j >= this.getColsCount() || i < 0 || j < 0) {
            throw new IndexOutOfBoundsException();
        }
    }

}
