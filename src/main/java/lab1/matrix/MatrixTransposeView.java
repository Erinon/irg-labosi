package lab1.matrix;

public class MatrixTransposeView extends AbstractMatrix implements IMatrix {

    private IMatrix matrix;

    public MatrixTransposeView(IMatrix matrix) {
        this.matrix = matrix;
    }

    @Override
    public int getRowsCount() {
        return matrix.getColsCount();
    }

    @Override
    public int getColsCount() {
        return matrix.getRowsCount();
    }

    @Override
    public double get(int i, int j) {
        checkIndexes(i, j);

        return matrix.get(j, i);
    }

    @Override
    public IMatrix set(int i, int j, double x) {
        checkIndexes(i, j);

        return matrix.set(j, i, x);
    }

    @Override
    public IMatrix copy() {
        return new MatrixTransposeView(this.matrix.copy());
    }

    @Override
    public IMatrix newInstance(int rows, int cols) {
        return this.matrix.newInstance(rows, cols);
    }

    private void checkIndexes(int i, int j) {
        if (i >= this.getRowsCount() || j >= this.getColsCount() || i < 0 || j < 0) {
            throw new IndexOutOfBoundsException();
        }
    }

}
