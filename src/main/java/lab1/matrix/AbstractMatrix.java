package lab1.matrix;

import lab1.helpers.VectorMatrixView;
import lab1.vector.IVector;

public abstract class AbstractMatrix implements IMatrix {

    public AbstractMatrix() {
    }

    @Override
    public IMatrix nTranspose(boolean liveView) {
        if (liveView) {
            return new MatrixTransposeView(this);
        } else {
            return new MatrixTransposeView(this.copy());
        }
    }

    @Override
    public IMatrix add(IMatrix other) {
        if (this.getRowsCount() != other.getRowsCount() ||
            this.getColsCount() != other.getColsCount()) {
            throw new IllegalArgumentException();
        }

        for (int i = this.getRowsCount() - 1; i >= 0; i--) {
            for (int j = this.getColsCount() - 1; j >= 0; j--) {
                this.set(i, j, this.get(i, j) + other.get(i, j));
            }
        }

        return this;
    }

    @Override
    public IMatrix nAdd(IMatrix other) {
        return this.copy().add(other);
    }

    @Override
    public IMatrix sub(IMatrix other) {
        if (this.getRowsCount() != other.getRowsCount() ||
                this.getColsCount() != other.getColsCount()) {
            throw new IllegalArgumentException();
        }

        for (int i = this.getRowsCount() - 1; i >= 0; i--) {
            for (int j = this.getColsCount() - 1; j >= 0; j--) {
                this.set(i, j, this.get(i, j) - other.get(i, j));
            }
        }

        return this;
    }

    @Override
    public IMatrix nSub(IMatrix other) {
        return this.copy().sub(other);
    }

    @Override
    public IMatrix nMultiply(IMatrix other) {
        if (this.getColsCount() != other.getRowsCount()) {
            throw new IllegalArgumentException();
        }

        IMatrix matrix = this.newInstance(this.getRowsCount(), other.getColsCount());

        double value;

        for (int i = 0, r = this.getRowsCount(); i < r; i++) {
            for (int j = 0, c = other.getColsCount(); j < c; j++) {
                value = 0.;

                for (int k = this.getColsCount() - 1; k >= 0; k--) {
                    value += this.get(i, k) * other.get(k, j);
                }

                matrix.set(i, j, value);
            }
        }

        return matrix;
    }

    @Override
    public double determinant() {
        int r = this.getRowsCount();

        if (r != this.getColsCount()) {
            throw new IllegalArgumentException();
        }

        if (r == 0) {
            return 0.;
        }

        if (r == 1) {
            return this.get(0, 0);
        }

        double d = 0.;

        int sign = 1;

        for (int i = 0; i < r; i++) {
            d += sign * this.get(i, 0) * (new MatrixSubMatrixView(this, i, 0)).determinant();

            sign *= -1;
        }

        return d;
    }

    @Override
    public IMatrix subMatrix(int row, int col, boolean liveView) {
        if (liveView) {
            return new MatrixSubMatrixView(this, row, col);
        } else {
            return new MatrixSubMatrixView(this.copy(), row, col);
        }
    }

    @Override
    public IMatrix nInvert() {
        int r = this.getRowsCount();
        int c = this.getColsCount();

        double det = this.determinant();

        if (det == 0) {
            throw new IllegalArgumentException();
        }

        double invDet = 1. / det;

        IMatrix matrix = this.newInstance(r, c);

        int sign;

        for (int i = 0; i < r; i++) {
            sign = ((i % 2) == 0) ? 1 : -1;

            for (int j = 0; j < c; j++) {
                matrix.set(i, j, sign * invDet * (new MatrixSubMatrixView(this, i, j)).determinant());

                sign *= -1;
            }
        }

        return new MatrixTransposeView(matrix);
    }

    @Override
    public double[][] toArray() {
        double[][] arr = new double[this.getRowsCount()][this.getColsCount()];

        for (int i = this.getRowsCount(); i >= 0; i--) {
            for (int j = this.getColsCount(); j >= 0; j--) {
                arr[i][j] = this.get(i, j);
            }
        }

        return arr;
    }

    @Override
    public IVector toVector(boolean liveView) {
        if (liveView) {
            return new VectorMatrixView(this);
        } else {
            return new VectorMatrixView(this.copy());
        }
    }

    public String toString() {
        return toString(3);
    }

    public String toString(int precision) {
        if (precision < 0) {
            throw new IllegalArgumentException();
        }

        if (this.getRowsCount() == 0) {
            return "[ ]";
        }

        int c = this.getColsCount();

        String format = "%." + precision + "f";
        StringBuilder sb = new StringBuilder();

        for (int i = 0, r = this.getRowsCount(); i < r; i++) {
            sb.append('[');

            if (c == 0) {
                sb.append(" ]\n");

                continue;
            }

            sb.append(String.format(format, this.get(i, 0)));

            for (int j = 1; j < c; j++) {
                sb.append(", ");
                sb.append(String.format(format, this.get(i, j)));
            }

            sb.append("]\n");
        }

        sb.deleteCharAt(sb.length() - 1);

        return sb.toString();
    }

}
