package lab1.matrix;

import javax.swing.*;
import java.util.Arrays;

public class Matrix extends AbstractMatrix {

    private double[][] elements;
    private int rows;
    private int cols;

    public Matrix(int rows, int cols) {
        if (rows < 0 || cols < 0) {
            throw new IllegalArgumentException();
        }

        this.elements = new double[rows][cols];
        this.rows = rows;
        this.cols = cols;
    }

    public Matrix(double[][] elements, boolean elementsOwner) {
        if (elementsOwner) {
            this.elements = elements;
        } else {
            this.elements = new double[elements.length][];

            for (int i = 0, l = elements.length; i < l; i++) {
                this.elements[i] = elements[i].clone();
            }
        }

        this.rows = this.elements.length;

        if (rows == 0) {
            cols = 0;
        } else {
            this.cols = this.elements[0].length;
        }

        for (int i = 0; i < rows; i++) {
            if (this.elements[i].length != cols) {
                throw new IllegalArgumentException();
            }
        }
    }

    public static IMatrix getIdentityMatrix(int n) {
        IMatrix I = new Matrix(n, n);

        for (int i = 0; i < n; i++) {
            I.set(i, i, 1);
        }

        return I;
    }

    @Override
    public int getRowsCount() {
        return rows;
    }

    @Override
    public int getColsCount() {
        return cols;
    }

    @Override
    public double get(int i, int j) {
        checkIndexes(i, j);

        return this.elements[i][j];
    }

    @Override
    public IMatrix set(int i, int j, double x) {
        checkIndexes(i, j);

        this.elements[i][j] = x;

        return this;
    }

    @Override
    public IMatrix copy() {
        return new Matrix(elements, false);
    }

    @Override
    public IMatrix newInstance(int rows, int cols) {
        return new Matrix(rows, cols);
    }

    public static IMatrix parseSimple(String matrix) {
        if (matrix.trim().isEmpty()) {
            return new Matrix(0, 0);
        }

        String[] splMatrix = matrix.split("\\|");

        int r = splMatrix.length;

        double[][] arr = new double[r][];

        if (splMatrix[0].trim().isEmpty()) {
            arr[0] = new double[0];
        } else {
            arr[0] = Arrays.stream(splMatrix[0].trim().split("\\s+"))
                    .mapToDouble(Double::parseDouble)
                    .toArray();
        }

        int c = arr[0].length;

        for (int i = 1; i < r; i++) {
            if (splMatrix[i].trim().isEmpty()) {
                arr[i] = new double[0];
            } else {
                arr[i] = Arrays.stream(splMatrix[i].trim().split("\\s+"))
                        .mapToDouble(Double::parseDouble)
                        .toArray();
            }

            if (arr[i].length != c) {
                throw new IllegalArgumentException();
            }
        }

        return new Matrix(arr, false);
    }

    private void checkIndexes(int i, int j) {
        if (i >= rows || j >= cols || i < 0 || j < 0) {
            throw new IndexOutOfBoundsException();
        }
    }

}
