package lab7;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import lab1.IRG;
import lab1.vector.IVector;
import lab1.vector.Vector;
import lab5.Face3D;
import lab5.ObjectModel;
import lab5.Vertex3D;
import lab6.Drawer.CustomDrawer;
import lab6.Drawer.Drawer;
import lab6.Model;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

@SuppressWarnings("Duplicates")
public class CustomRemoval {

    static {
        GLProfile.initSingleton();
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Please provide object file name as only argument.");

            System.exit(1);
        }

        if (!args[0].endsWith(".obj")) {
            System.err.println("Please provide only .obj file type.");
        }

        ObjectModel objectModel = new ObjectModel(args[0]);
        objectModel.normalize(false);

        Model model = new Model();
        Drawer drawer = new CustomDrawer();

        Mode mode = new Mode();

        SwingUtilities.invokeLater(() -> {
            GLProfile glProfile = GLProfile.getDefault();
            GLCapabilities glCapabilities = new GLCapabilities(glProfile);
            final GLCanvas glCanvas = new GLCanvas(glCapabilities);
            //glCapabilities.setDoubleBuffered(true);

            glCanvas.addKeyListener(new KeyAdapter() {
                @Override
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_R) {
                        e.consume();

                        model.incrementAngle();

                        glCanvas.display();
                    } else if (e.getKeyCode() == KeyEvent.VK_L) {
                        e.consume();

                        model.decrementAngle();

                        glCanvas.display();
                    } else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                        e.consume();

                        model.resetAngle();

                        glCanvas.display();
                    } else if (e.getKeyCode() == KeyEvent.VK_1) {
                        e.consume();

                        mode.setMode(1);

                        glCanvas.display();
                    } else if (e.getKeyCode() == KeyEvent.VK_2) {
                        e.consume();

                        mode.setMode(2);

                        glCanvas.display();
                    } else if (e.getKeyCode() == KeyEvent.VK_3) {
                        e.consume();

                        mode.setMode(3);

                        glCanvas.display();
                    } else if (e.getKeyCode() == KeyEvent.VK_4) {
                        e.consume();

                        mode.setMode(4);

                        glCanvas.display();
                    }
                }
            });

            glCanvas.addGLEventListener(new GLEventListener() {
                @Override
                public void init(GLAutoDrawable glAutoDrawable) {
                }

                @Override
                public void dispose(GLAutoDrawable glAutoDrawable) {
                }

                @Override
                public void display(GLAutoDrawable glAutoDrawable) {
                    GL2 gl2 = glAutoDrawable.getGL().getGL2();
                    GLU glu = new GLU();

                    gl2.glClearColor(0, 1, 0, 0);
                    gl2.glClear(GL.GL_COLOR_BUFFER_BIT);
                    gl2.glLoadIdentity();

                    double eyeX = model.getR() * Math.cos(Math.toRadians(model.getAngle()));
                    double eyeY = 4.;
                    double eyeZ = model.getR() * Math.sin(Math.toRadians(model.getAngle()));

                    drawer.viewTransform(glu, model, eyeX, eyeY, eyeZ,
                            0., 0., 0.,
                            0., 1., 0.);

                    drawShape(gl2, objectModel, new Vector(eyeX, eyeY, eyeZ));

                    gl2.glFlush();
                    //glAutoDrawable.swapBuffers();
                }

                @Override
                public void reshape(GLAutoDrawable glAutoDrawable, int i, int i1, int i2, int i3) {
                    GL2 gl2 = glAutoDrawable.getGL().getGL2();
                    GLU glu = new GLU();

                    gl2.glMatrixMode(GL2.GL_PROJECTION);
                    gl2.glLoadIdentity();

                    drawer.persProjection(gl2, glu, model, -0.5, 0.5, -0.5, 0.5, 1, 100);

                    gl2.glMatrixMode(GL2.GL_MODELVIEW);
                    gl2.glViewport(0, 0, i2, i3);
                }

                private void drawShape(GL2 gl2, ObjectModel model, IVector eye) {
                    int m = mode.getMode();

                    if (m == 2) {
                        model.determineFaceVisibilities1(eye);
                    } else if (m == 3) {
                        model.determineFaceVisibilities2(eye);
                    }

                    gl2.glColor3f(1, 0, 0);

                    for (Face3D f : objectModel.getFaces()) {
                        if ((m == 2 || m == 3) && !f.isVisible()) {
                            continue;
                        }

                        List<Vertex3D> v = model.getFaceVertexes(f);

                        IVector v1  = new Vector(v.get(0).getX(), v.get(0).getY(), v.get(0).getZ(), 1.);
                        IVector v2  = new Vector(v.get(1).getX(), v.get(1).getY(), v.get(1).getZ(), 1.);
                        IVector v3  = new Vector(v.get(2).getX(), v.get(2).getY(), v.get(2).getZ(), 1.);

                        IVector tv1 = v1.toRowMatrix(false).nMultiply(drawer.getTransformationMatrix()).toVector(false).nFromHomogeneous();
                        IVector tv2 = v2.toRowMatrix(false).nMultiply(drawer.getTransformationMatrix()).toVector(false).nFromHomogeneous();
                        IVector tv3 = v3.toRowMatrix(false).nMultiply(drawer.getTransformationMatrix()).toVector(false).nFromHomogeneous();

                        if (m == 4 && !IRG.isAntiClockwise(tv1, tv2, tv3)) {
                            continue;
                        }

                        gl2.glBegin(GL2.GL_LINE_LOOP);

                        drawer.drawVertex(gl2, tv1);
                        drawer.drawVertex(gl2, tv2);
                        drawer.drawVertex(gl2, tv3);

                        gl2.glEnd();
                    }
                }

            });

            final JFrame jFrame = new JFrame("Projections");
            jFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
            jFrame.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    jFrame.dispose();
                    System.exit(0);
                }
            });
            jFrame.getContentPane().add(glCanvas, BorderLayout.CENTER);
            jFrame.setSize(640, 480);
            jFrame.setVisible(true);
            glCanvas.requestFocusInWindow();
        });

    }

}
