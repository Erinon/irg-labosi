package lab7;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import lab1.vector.IVector;
import lab1.vector.Vector;
import lab5.ObjectModel;
import lab5.Vertex3D;
import lab6.Drawer.Drawer;
import lab6.Drawer.FrustumDrawer;
import lab6.Model;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

@SuppressWarnings("Duplicates")
public class GLRemoval {

    static {
        GLProfile.initSingleton();
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Please provide object file name as only argument.");

            System.exit(1);
        }

        if (!args[0].endsWith(".obj")) {
            System.err.println("Please provide only .obj file type.");
        }

        ObjectModel objectModel = new ObjectModel(args[0]);
        objectModel.normalize(false);

        Model model = new Model();
        Drawer drawer = new FrustumDrawer();

        SwingUtilities.invokeLater(() -> {
            GLProfile glProfile = GLProfile.getDefault();
            GLCapabilities glCapabilities = new GLCapabilities(glProfile);
            final GLCanvas glCanvas = new GLCanvas(glCapabilities);
            //glCapabilities.setDoubleBuffered(true);

            glCanvas.addKeyListener(new KeyAdapter() {
                @Override
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_R) {
                        e.consume();

                        model.incrementAngle();

                        glCanvas.display();
                    } else if (e.getKeyCode() == KeyEvent.VK_L) {
                        e.consume();

                        model.decrementAngle();

                        glCanvas.display();
                    } else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                        e.consume();

                        model.resetAngle();

                        glCanvas.display();
                    }
                }
            });

            glCanvas.addGLEventListener(new GLEventListener() {
                @Override
                public void init(GLAutoDrawable glAutoDrawable) {
                }

                @Override
                public void dispose(GLAutoDrawable glAutoDrawable) {
                }

                @Override
                public void display(GLAutoDrawable glAutoDrawable) {
                    GL2 gl2 = glAutoDrawable.getGL().getGL2();
                    GLU glu = new GLU();

                    gl2.glClearColor(0, 1, 0, 0);
                    gl2.glClear(GL.GL_COLOR_BUFFER_BIT);
                    gl2.glLoadIdentity();

                    drawer.viewTransform(glu, model, model.getR() * Math.cos(Math.toRadians(model.getAngle())), 4, model.getR() * Math.sin(Math.toRadians(model.getAngle())),
                            0, 0, 0,
                            0, 1, 0);

                    drawShape(gl2, objectModel);

                    gl2.glFlush();
                    //glAutoDrawable.swapBuffers();
                }

                @Override
                public void reshape(GLAutoDrawable glAutoDrawable, int i, int i1, int i2, int i3) {
                    GL2 gl2 = glAutoDrawable.getGL().getGL2();
                    GLU glu = new GLU();

                    gl2.glMatrixMode(GL2.GL_PROJECTION);
                    gl2.glLoadIdentity();

                    drawer.persProjection(gl2, glu, model, -0.5, 0.5, -0.5, 0.5, 1, 100);

                    gl2.glMatrixMode(GL2.GL_MODELVIEW);
                    gl2.glViewport(0, 0, i2, i3);
                }

                private void drawShape(GL2 gl2, ObjectModel model) {
                    List<Vertex3D> v = model.getOrderedVertexes();

                    gl2.glColor3f(1, 0, 0);
                    gl2.glPolygonMode(GL2.GL_FRONT, GL2.GL_LINE);
                    gl2.glEnable(GL2.GL_CULL_FACE);
                    gl2.glCullFace(GL2.GL_BACK);

                    for (int i = 0, l = v.size(); i < l; i += 3) {
                        gl2.glBegin(GL2.GL_POLYGON);

                        IVector v1  = new Vector(v.get(i).getX(), v.get(i).getY(), v.get(i).getZ(), 1.);
                        IVector v2  = new Vector(v.get(i + 1).getX(), v.get(i + 1).getY(), v.get(i + 1).getZ(), 1.);
                        IVector v3  = new Vector(v.get(i + 2).getX(), v.get(i + 2).getY(), v.get(i + 2).getZ(), 1.);

                        IVector tv1 = v1.toRowMatrix(false).nMultiply(drawer.getTransformationMatrix()).toVector(false).nFromHomogeneous();
                        IVector tv2 = v2.toRowMatrix(false).nMultiply(drawer.getTransformationMatrix()).toVector(false).nFromHomogeneous();
                        IVector tv3 = v3.toRowMatrix(false).nMultiply(drawer.getTransformationMatrix()).toVector(false).nFromHomogeneous();

                        drawer.drawVertex(gl2, tv1);
                        drawer.drawVertex(gl2, tv2);
                        drawer.drawVertex(gl2, tv3);

                        gl2.glEnd();
                    }
                }

            });

            final JFrame jFrame = new JFrame("Uklanjanje skrivenih poligona");
            jFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
            jFrame.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    jFrame.dispose();
                    System.exit(0);
                }
            });
            jFrame.getContentPane().add(glCanvas, BorderLayout.CENTER);
            jFrame.setSize(640, 480);
            jFrame.setVisible(true);
            glCanvas.requestFocusInWindow();
        });

    }

}
