package lab7;

public class Mode {

    private int mode;

    public Mode() {
        setMode(1);
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;

        switch (mode) {
            case 1:
                System.out.println("Bez odbacivanja.");
                break;
            case 2:
                System.out.println("Odbacivanja algoritmom 1.");
                break;
            case 3:
                System.out.println("Odbacivanja algoritmom 2.");
                break;
            case 4:
                System.out.println("Odbacivanja algoritmom 3.");
                break;
            default:
                break;
        }
    }

}
