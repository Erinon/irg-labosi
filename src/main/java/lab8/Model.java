package lab8;

import java.util.LinkedList;
import java.util.List;

public class Model {

    private List<Vertex2D> vertexes;
    private int divs;
    private Vertex2D picked;

    public Model() {
        this.vertexes = new LinkedList<>();
        this.divs = 50;
        this.picked = null;
    }

    private double distance(Vertex2D v, double x, double y) {
        return Math.sqrt((v.getX() - x) * (v.getX() - x) + (v.getY() - y) * (v.getY() - y));
    }

    public void releasePoint() {
        picked = null;
    }

    public void mouseMoved(double x, double y) {
        if (picked != null) {
            picked.setX(x);
            picked.setY(y);
        }
    }

    public void pickPoint(double x, double y) {
        double minDist = -1;
        Vertex2D max = null;

        for (Vertex2D v : vertexes) {
            double dist = distance(v, x, y);

            if (max == null || dist < minDist) {
                max = v;
                minDist = dist;
            }
        }

        this.picked = max;
    }

    public void reset() {
        vertexes.clear();
    }

    public void addPoint(double x, double y) {
        vertexes.add(new Vertex2D(x, y));
    }

    public List<Vertex2D> getPoints() {
        return vertexes;
    }

    public int getDivs() {
        return divs;
    }

    public void setDivs(int divs) {
        this.divs = divs;
    }

}
