package lab8;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import lab1.matrix.IMatrix;
import lab1.matrix.Matrix;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("Duplicates")
public class BezierDrawer {

    static {
        GLProfile.initSingleton();
    }

    public static void main(String[] args) {

        SwingUtilities.invokeLater(() -> {
            GLProfile glProfile = GLProfile.getDefault();
            GLCapabilities glCapabilities = new GLCapabilities(glProfile);
            final GLCanvas glCanvas = new GLCanvas(glCapabilities);

            Model model = new Model();

            glCanvas.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if (e.getButton() == MouseEvent.BUTTON1) {
                        model.addPoint(e.getX(), e.getY());
                    }

                    glCanvas.display();
                }

                @Override
                public void mousePressed(MouseEvent e) {
                    if (e.getButton() == MouseEvent.BUTTON3) {
                        model.pickPoint(e.getX(), e.getY());
                    }

                    glCanvas.display();
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                    if (e.getButton() == MouseEvent.BUTTON3) {
                        model.releasePoint();
                    }

                    glCanvas.display();
                }
            });

            glCanvas.addMouseMotionListener(new MouseMotionAdapter() {
                @Override
                public void mouseDragged(MouseEvent e) {
                    model.mouseMoved(e.getX(), e.getY());

                    glCanvas.display();
                }
            });

            glCanvas.addKeyListener(new KeyAdapter() {
                @Override
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                        e.consume();

                        model.reset();

                        glCanvas.display();
                    }
                }
            });

            glCanvas.addGLEventListener(new GLEventListener() {
                @Override
                public void init(GLAutoDrawable glAutoDrawable) {
                }

                @Override
                public void dispose(GLAutoDrawable glAutoDrawable) {
                }

                @Override
                public void display(GLAutoDrawable glAutoDrawable) {
                    GL2 gl2 = glAutoDrawable.getGL().getGL2();

                    gl2.glClearColor(0, 1, 0, 0);
                    gl2.glClear(GL.GL_COLOR_BUFFER_BIT);
                    gl2.glLoadIdentity();

                    gl2.glColor3f(1, 0, 0);
                    drawPolygon(gl2, model.getPoints());

                    gl2.glColor3f(0, 0, 1);
                    aproxBezier(gl2, model.getPoints(), model.getDivs());

                    gl2.glColor3f(0, 0, 0);
                    interpolatedBezier(gl2, model.getPoints(), model.getDivs());

                    gl2.glFlush();
                }

                @Override
                public void reshape(GLAutoDrawable glAutoDrawable, int i, int i1, int i2, int i3) {
                    GL2 gl2 = glAutoDrawable.getGL().getGL2();
                    GLU glu = new GLU();

                    gl2.glMatrixMode(GL2.GL_PROJECTION);
                    gl2.glLoadIdentity();

                    glu.gluOrtho2D(0.0f, i2, i3, 0.0f);

                    gl2.glMatrixMode(GL2.GL_MODELVIEW);
                    gl2.glLoadIdentity();
                    gl2.glViewport(0, 0, i2, i3);
                }

                private void drawPolygon(GL2 gl2, List<Vertex2D> points) {
                    gl2.glBegin(GL2.GL_LINE_STRIP);

                    for (Vertex2D p : points) {
                        gl2.glVertex2f((float)p.getX(), (float)p.getY());
                    }

                    gl2.glEnd();
                }

                private int[] computeFactors(int n) {
                    int[] factors = new int[n + 1];
                    int a = 1;

                    for (int i = 0; i < n + 1; i++) {
                        factors[i] = a;

                        a = a * (n - i) / (i + 1);
                    }

                    return factors;
                }

                private void aproxBezier(GL2 gl2, List<Vertex2D> points, int divs) {
                    int n = points.size() - 1;

                    if (n < 0) {
                        return;
                    }

                    double t, b;

                    Vertex2D p = new Vertex2D();

                    int[] factors = computeFactors(n);

                    gl2.glBegin(GL2.GL_LINE_STRIP);

                    for (int i = 0; i <= divs; i++) {
                        t = 1. / divs * i;

                        p.setX(0.);
                        p.setY(0.);

                        for (int j = 0; j <= n; j++) {
                            b = factors[j];

                            if (j == 0) {
                                b *= Math.pow(1 - t, n);
                            } else if (j == n) {
                                b *= Math.pow(t, n);
                            } else {
                                b *= Math.pow(t, j) * Math.pow(1 - t, n - j);
                            }

                            p.setX(p.getX() + b * points.get(j).getX());
                            p.setY(p.getY() + b * points.get(j).getY());
                        }

                        gl2.glVertex2f((float)p.getX(), (float)p.getY());
                    }

                    gl2.glEnd();
                }

                private List<Vertex2D> interpolatedPoints(List<Vertex2D> points) {
                    int n = points.size() - 1;

                    if (n < 0) {
                        return new LinkedList<>();
                    }

                    double t, b;

                    int[] factors = computeFactors(n);

                    IMatrix tb = new Matrix(n + 1, n + 1);

                    for (int i = 0; i <= n; i++) {
                        for (int j = 0; j <= n; j++) {
                            t = 1. * i / n;
                            b = factors[j];

                            if (j == 0) {
                                b *= Math.pow(1 - t, n);
                            } else if (j == n) {
                                b *= Math.pow(t, n);
                            } else {
                                b *= Math.pow(t, j) * Math.pow(1 - t, n - j);
                            }

                            tb.set(i, j, b);
                        }
                    }

                    IMatrix p = new Matrix(n + 1, 2);

                    for (int i = 0; i < n + 1; i++) {
                        Vertex2D v = points.get(i);

                        p.set(i, 0, v.getX());
                        p.set(i, 1, v.getY());
                    }

                    IMatrix r = tb.nInvert().nMultiply(p);

                    List<Vertex2D> newPoints = new LinkedList<>();

                    for (int i = 0; i < n + 1; i++) {
                        newPoints.add(new Vertex2D(r.get(i, 0), r.get(i, 1)));
                    }

                    return newPoints;
                }

                private void interpolatedBezier(GL2 gl2, List<Vertex2D> points, int divs) {
                    aproxBezier(gl2, interpolatedPoints(points), divs);
                }

            });

            final JFrame jFrame = new JFrame("Bezierove krivulje");
            jFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
            jFrame.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    jFrame.dispose();
                    System.exit(0);
                }
            });
            jFrame.getContentPane().add(glCanvas, BorderLayout.CENTER);
            jFrame.setSize(640, 480);
            jFrame.setVisible(true);
            glCanvas.requestFocusInWindow();
        });

    }

}
