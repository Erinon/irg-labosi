package lab6;

import lab1.vector.IVector;
import lab1.vector.Vector;

public class Model {

    private double initialAngle;
    private double angle;
    private double increment;
    private double r;
    private boolean constant;
    private boolean z;
    private IVector lightVector;
    private IVector ambL;
    private IVector diffL;
    private IVector specL;
    private IVector ambM;
    private IVector diffM;
    private IVector specM;
    private double shininess;
    private IVector eye;

    public Model() {
        initialAngle = 18.4349488;
        angle = initialAngle;
        increment = 1.;
        r = 3.16227766;
        constant = true;
        calculateEye();
        z = false;

        System.out.println("Z-spremnik isključen");
    }

    private double distance(IVector p1, IVector p2) {
        return Math.sqrt(Math.pow(p1.get(0) - p2.get(0), 2) + Math.pow(p1.get(1) - p2.get(1), 2) + Math.pow(p1.get(2) - p2.get(2), 2));
    }

    public double[] intensities(IVector n, IVector p) {
        //System.out.println(n.getDimension() + " " + p.getDimension() + " " + lightVector.getDimension());
        IVector l = lightVector.nSub(p).nNormalize();
        IVector v = eye.nSub(p).nNormalize();
        IVector r = n.nNormalize().nScalarMultiply(l.scalarProduct(n)).nScalarMultiply(2.).nSub(l).nNormalize();

        double ln = Math.max(l.scalarProduct(n), 0);
        double rv = Math.pow(r.scalarProduct(v), shininess);

        //System.out.println(ln + " " + rv);

        double Ir = ambL.get(0) * ambM.get(0) + (diffL.get(0) * diffM.get(0) * ln + specL.get(0) * specM.get(0) * rv);
        double Ig = ambL.get(1) * ambM.get(1) + (diffL.get(1) * diffM.get(1) * ln + specL.get(1) * specM.get(1) * rv);
        double Ib = ambL.get(2) * ambM.get(2) + (diffL.get(2) * diffM.get(2) * ln + specL.get(2) * specM.get(2) * rv);

        return new double[] {Ir, Ig, Ib};
    }

    public void toogleZ() {
        z ^= true;

        if (z) {
            System.out.println("Z-spremnik uključen");
        } else {
            System.out.println("Z-spremnik isključen");
        }
    }

    private void calculateEye() {
        eye = new Vector(
                r * Math.cos(Math.toRadians(angle)),
                4,
                r * Math.sin(Math.toRadians(angle))
        );
    }

    public void setShininess(double shininess) {
        this.shininess = shininess;
    }

    public void setLightVector(double x, double y, double z, double h) {
        lightVector = new Vector(x, y, z, h).copyPart(3);
    }

    public void setAmbL(double x, double y, double z, double h) {
        this.ambL = new Vector(x, y, z, h);
    }

    public void setDiffL(double x, double y, double z, double h) {
        this.diffL = new Vector(x, y, z, h);
    }

    public void setSpecL(double x, double y, double z, double h) {
        this.specL = new Vector(x, y, z, h);
    }

    public void setAmbM(double x, double y, double z, double h) {
        this.ambM = new Vector(x, y, z, h);
    }

    public void setDiffM(double x, double y, double z, double h) {
        this.diffM = new Vector(x, y, z, h);
    }

    public void setSpecM(double x, double y, double z, double h) {
        this.specM = new Vector(x, y, z, h);
    }

    public boolean isZ() {
        return z;
    }

    public void setConstant(boolean constant) {
        this.constant = constant;
    }

    public boolean isConstant() {
        return constant;
    }

    public void incrementAngle() {
        angle += increment;
        calculateEye();
    }

    public void decrementAngle() {
        angle -= increment;
        calculateEye();
    }

    public void resetAngle() {
        angle = initialAngle;
        calculateEye();
    }

    public double getAngle() {
        return angle;
    }

    public double getR() {
        return r;
    }

    public IVector getEye() {
        return eye;
    }

}
