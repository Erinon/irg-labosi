package lab6.Drawer;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.glu.GLU;
import lab1.matrix.IMatrix;
import lab1.vector.IVector;
import lab6.Model;

public interface Drawer {

    void viewTransform(GLU glu, Model model, double eyeX, double eyeY, double eyeZ,
                       double centerX, double centerY, double centerZ,
                       double upX, double upY, double upZ);

    void persProjection(GL2 gl2, GLU glu, Model model, double l, double r, double b, double t, double n, double f);

    IMatrix getTransformationMatrix();

    void drawVertex(GL2 gl2, IVector v);

}
