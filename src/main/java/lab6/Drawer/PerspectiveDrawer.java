package lab6.Drawer;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.glu.GLU;
import lab1.matrix.IMatrix;
import lab1.matrix.Matrix;
import lab1.vector.IVector;
import lab6.Model;

public class PerspectiveDrawer implements Drawer {

    @Override
    public void viewTransform(GLU glu, Model model, double eyeX, double eyeY, double eyeZ, double centerX, double centerY, double centerZ, double upX, double upY, double upZ) {
        glu.gluLookAt(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ);
    }

    @Override
    public void persProjection(GL2 gl2, GLU glu, Model model, double l, double r, double b, double t, double n, double f) {
        glu.gluPerspective(Math.toDegrees(2 * Math.atan((t - b) / (2.*n))), (r - l) / (t - b), n, f);
    }

    @Override
    public IMatrix getTransformationMatrix() {
        return Matrix.getIdentityMatrix(4);
    }

    @Override
    public void drawVertex(GL2 gl2, IVector v) {
        gl2.glVertex3f((float)v.get(0), (float)v.get(1), (float)v.get(2));
    }

}
