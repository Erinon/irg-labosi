package lab6.Drawer;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.glu.GLU;
import lab1.IRG;
import lab1.matrix.IMatrix;
import lab1.matrix.Matrix;
import lab1.vector.IVector;
import lab1.vector.Vector;
import lab6.Model;

public class CustomDrawer implements Drawer {

    private IMatrix tp;
    private IMatrix pr;
    private IMatrix m;

    public CustomDrawer() {
        tp = Matrix.getIdentityMatrix(4);
        pr = Matrix.getIdentityMatrix(4);
        m = Matrix.getIdentityMatrix(4);
    }

    @Override
    public void viewTransform(GLU glu, Model model, double eyeX, double eyeY, double eyeZ, double centerX, double centerY, double centerZ, double upX, double upY, double upZ) {
        tp = IRG.lookAtMatrix(new Vector(eyeX, eyeY, eyeZ), new Vector(centerX, centerY, centerZ), new Vector(upX, upY, upZ));

        m = tp.nMultiply(pr);
    }

    @Override
    public void persProjection(GL2 gl2, GLU glu, Model model, double l, double r, double b, double t, double n, double f) {
        pr = IRG.buildFrustumMatrix(l, r, b, t, n, f);

        m = tp.nMultiply(pr);
    }

    @Override
    public IMatrix getTransformationMatrix() {
        return m;
    }

    @Override
    public void drawVertex(GL2 gl2, IVector v) {
        gl2.glVertex2f((float)v.get(0), (float)v.get(1));
    }

}
