package lab3;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import lab3.model.Line;
import lab3.model.Model;
import lab3.model.Point;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

@SuppressWarnings("Duplicates")
public class LineDrawer {

    static {
        GLProfile.initSingleton();
    }

    public static void main(String[] args) {

        SwingUtilities.invokeLater(() -> {
            GLProfile glProfile = GLProfile.getDefault();
            GLCapabilities glCapabilities = new GLCapabilities(glProfile);
            final GLCanvas glCanvas = new GLCanvas(glCapabilities);

            Model model = new Model(640, 480);

            float[] black = new float[] {0, 0, 0};
            float[] red = new float[] {1, 0, 0};
            float[] green = new float[] {0, 1, 0};

            glCanvas.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    model.addPoint(e.getX(), e.getY());

                    glCanvas.display();
                }
            });

            glCanvas.addKeyListener(new KeyAdapter() {
                @Override
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_O) {
                        e.consume();

                        model.invertCrop();

                        glCanvas.display();
                    } else if (e.getKeyCode() == KeyEvent.VK_K) {
                        e.consume();

                        model.invertControl();

                        glCanvas.display();
                    }
                }
            });

            glCanvas.addGLEventListener(new GLEventListener() {
                @Override
                public void init(GLAutoDrawable glAutoDrawable) {
                }

                @Override
                public void dispose(GLAutoDrawable glAutoDrawable) {
                }

                @Override
                public void display(GLAutoDrawable glAutoDrawable) {
                    GL2 gl2 = glAutoDrawable.getGL().getGL2();

                    gl2.glClearColor(1, 1, 1, 1);
                    gl2.glClear(GL.GL_COLOR_BUFFER_BIT);

                    gl2.glLoadIdentity();

                    if (model.isCrop()) {
                        drawRectangle(gl2, model.getCropMin(), model.getCropMax(), green);
                    }

                    for (Line l : model.getLines()) {
                        if (model.isCrop()) {
                            Line nL = sutherlandCalculateLine(l);

                            if (nL != null) {
                                bresenhamDrawLine(gl2, nL.getA().getX(), nL.getA().getY(),
                                        nL.getB().getX(), nL.getB().getY(), black);
                            }
                        } else {
                            bresenhamDrawLine(gl2, l.getA().getX(), l.getA().getY(),
                                    l.getB().getX(), l.getB().getY(), black);
                        }

                        if (model.isControl()) {
                            bresenhamDrawLine(gl2, l.getpA().getX(), l.getpA().getY(),
                                    l.getpB().getX(), l.getpB().getY(), red);
                        }
                    }

                    gl2.glFlush();
                }

                @Override
                public void reshape(GLAutoDrawable glAutoDrawable, int i, int i1, int i2, int i3) {
                    GL2 gl2 = glAutoDrawable.getGL().getGL2();
                    gl2.glMatrixMode(GL2.GL_PROJECTION);
                    gl2.glLoadIdentity();

                    GLU glu = new GLU();
                    glu.gluOrtho2D(0.0f, i2, i3, 0.0f);

                    gl2.glMatrixMode(GL2.GL_MODELVIEW);
                    gl2.glLoadIdentity();

                    gl2.glViewport(0, 0, i2, i3);

                    model.setWidth(i2);
                    model.setHeight(i3);
                }

                private void drawRectangle(GL2 gl2, Point start, Point end, float[] color) {
                    gl2.glBegin(GL2.GL_LINE_LOOP);
                    gl2.glColor3f(color[0], color[1], color[2]);
                    gl2.glVertex2f(start.getX(), start.getY());
                    gl2.glVertex2f(start.getX(), end.getY());
                    gl2.glVertex2f(end.getX(), end.getY());
                    gl2.glVertex2f(end.getX(), start.getY());
                    gl2.glEnd();
                }

                private Line sutherlandCalculateLine(Line l) {
                    Point a = l.getA();
                    Point b = l.getB();

                    a.generateCode(model.getCropMin(), model.getCropMax());
                    b.generateCode(model.getCropMin(), model.getCropMax());

                    /*for (int i = 0; i < 4; i++) {
                        System.out.println(a.getCode()[i]);
                    }

                    for (int i = 0; i < 4; i++) {
                        System.out.println(b.getCode()[i]);
                    }*/

                    if (a.getFirstTrueInCode() == -1 &&
                        b.getFirstTrueInCode() == -1) {
                        //System.out.println("unutra");
                        return l;
                    }

                    if (!isAndOfCodesZero(a, b)) {
                        //System.out.println("Vani");
                        return null;
                    }

                    Point nA = a.clone();
                    Point nB = b.clone();

                    double k = ((double)(a.getY() - b.getY())) / (a.getX() - b.getX());
                    double ll = a.getY() - k * a.getX();

                    nA.generateCode(model.getCropMin(), model.getCropMax());
                    nB.generateCode(model.getCropMin(), model.getCropMax());

                    int br = 0;

                    while (nA.getFirstTrueInCode() >= 0 && br < 5) {
                        for (int i = 0; i < 4; i++) {
                            if (nA.getCode()[i]) {
                                if (i == 0) {
                                    nA.setY(model.getCropMax().getY());
                                    nA.setX((int) ((nA.getY() - ll) / k));
                                } else if (i == 1) {
                                    nA.setY(model.getCropMin().getY());
                                    nA.setX((int) ((nA.getY() - ll) / k));
                                } else if (i == 2) {
                                    nA.setX(model.getCropMax().getX());
                                    nA.setY((int) (k * nA.getX() + ll));
                                } else if (i == 3) {
                                    nA.setX(model.getCropMin().getX());
                                    nA.setY((int) (k * nA.getX() + ll));
                                }

                                break;
                            }
                        }

                        br++;

                        nA.generateCode(model.getCropMin(), model.getCropMax());
                    }

                    if (br >= 5) {
                        return null;
                    }

                    br = 0;

                    while (nB.getFirstTrueInCode() >= 0 && br < 5) {
                        for (int i = 0; i < 4; i++) {
                            if (nB.getCode()[i]) {
                                if (i == 0) {
                                    nB.setY(model.getCropMax().getY());
                                    nB.setX((int) ((nB.getY() - ll) / k));
                                } else if (i == 1) {
                                    nB.setY(model.getCropMin().getY());
                                    nB.setX((int) ((nB.getY() - ll) / k));
                                } else if (i == 2) {
                                    nB.setX(model.getCropMax().getX());
                                    nB.setY((int) (k * nB.getX() + ll));
                                } else if (i == 3) {
                                    nB.setX(model.getCropMin().getX());
                                    nB.setY((int) (k * nB.getX() + ll));
                                }

                                break;
                            }
                        }

                        br++;

                        nB.generateCode(model.getCropMin(), model.getCropMax());
                    }

                    if (br >= 5) {
                        return null;
                    }

                    return new Line(nA, nB);
                }

                private boolean isAndOfCodesZero(Point a, Point b) {
                    boolean[] cA = a.getCode();
                    boolean[] cB = b.getCode();

                    for (int i = 0; i < 4; i++) {
                        if (cA[i] && cB[i]) {
                            return false;
                        }
                    }

                    return true;
                }

                private void bresenhamDrawLine(GL2 gl2, int xs, int ys, int xe, int ye, float[] color) {
                    gl2.glBegin(GL.GL_POINTS);
                    gl2.glColor3f(color[0], color[1], color[2]);

                    if (xs <= xe) {
                        if (ys <= ye) {
                            bresenhamDrawPositive(gl2, xs, ys, xe, ye);
                        } else {
                            bresenhamDrawNegative(gl2, xs, ys, xe, ye);
                        }
                    } else {
                        if (ys >= ye) {
                            bresenhamDrawPositive(gl2, xe, ye, xs, ys);
                        } else {
                            bresenhamDrawNegative(gl2, xe, ye, xs, ys);
                        }
                    }

                    gl2.glEnd();
                }

                private void bresenhamDrawPositive(GL2 gl2, int xs, int ys, int xe, int ye) {
                    int x, yc, corr;
                    int a, yf;

                    if (ye - ys <= xe - xs) {
                        a = 2 * (ye - ys);
                        yc = ys; yf = -(xe - xs); corr = -2 * (xe - xs);

                        for (x = xs; x <= xe; x++) {
                            gl2.glVertex2f(x, yc);

                            yf = yf + a;

                            if (yf > 0) {
                                yf = yf + corr;
                                yc = yc + 1;
                            }
                        }
                    } else {
                        x = xe; xe = ye; ye = x;
                        x = xs; xs = ys; ys = x;
                        a = 2 * (ye - ys);
                        yc = ys; yf = -(xe - xs); corr = -2 * (xe - xs);

                        for (x = xs; x <= xe; x++) {
                            gl2.glVertex2f(yc, x);

                            yf = yf + a;

                            if (yf >= 0) {
                                yf = yf + corr;
                                yc = yc + 1;
                            }
                        }
                    }
                }

                private void bresenhamDrawNegative(GL2 gl2, int xs, int ys, int xe, int ye) {
                    int x, yc, corr;
                    int a, yf;

                    if (-(ye - ys) <= xe - xs) {
                        a = 2 * (ye - ys);
                        yc = ys; yf = (xe - xs); corr = 2 * (xe - xs);

                        for (x = xs; x <= xe; x++) {
                            gl2.glVertex2f(x, yc);

                            yf = yf + a;

                            if (yf <= 0) {
                                yf = yf + corr;
                                yc = yc - 1;
                            }
                        }
                    } else {
                        x = xe; xe = ys; ys = x;
                        x = xs; xs = ye; ye = x;
                        a = 2 * (ye - ys);
                        yc = ys; yf = (xe - xs); corr = 2 * (xe - xs);

                        for (x = xs; x <= xe; x++) {
                            gl2.glVertex2f(yc, x);

                            yf = yf + a;

                            if (yf <= 0) {
                                yf = yf + corr;
                                yc = yc - 1;
                            }
                        }
                    }
                }
            });

            final JFrame jFrame = new JFrame("Primjer prikaza linija s odsijecanjem");
            jFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
            jFrame.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    jFrame.dispose();
                    System.exit(0);
                }
            });
            jFrame.getContentPane().add(glCanvas, BorderLayout.CENTER);
            jFrame.setSize(model.getWidth(), model.getHeight());
            jFrame.setVisible(true);
            glCanvas.requestFocusInWindow();
        });

    }

}
