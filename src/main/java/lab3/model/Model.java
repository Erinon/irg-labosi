package lab3.model;

import java.util.LinkedList;
import java.util.List;

public class Model {

    private List<Line> lines;
    private List<Point> points;
    private int width;
    private int height;
    private Point cropMin;
    private Point cropMax;
    private boolean control;
    private boolean crop;

    public Model(int width, int height) {
        this.lines = new LinkedList<>();
        this.points = new LinkedList<>();
        this.width = width;
        this.height = height;
        this.cropMin = new Point(width / 4, height / 4);
        this.cropMax = new Point(3 * width / 4, 3 * height / 4);
        this.control = false;
        this.crop = false;
    }

    public void invertControl() {
        control ^= true;
    }

    public void invertCrop() {
        crop ^= true;
    }

    public void addPoint(int x, int y) {
        if (points.size() >= 1) {
            lines.add(new Line(
                    points.get(0),
                    new Point(x, y))
            );

            points.clear();
        } else {
            points.add(new Point(x, y));
        }
    }

    public List<Line> getLines() {
        return lines;
    }

    public List<Point> getPoints() {
        return points;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Point getCropMin() {
        return cropMin;
    }

    public Point getCropMax() {
        return cropMax;
    }

    public boolean isControl() {
        return control;
    }

    public boolean isCrop() {
        return crop;
    }

    public void setWidth(int width) {
        this.width = width;
        this.cropMin.setX(width / 4);
        this.cropMax.setX(3 * width / 4);
    }

    public void setHeight(int height) {
        this.height = height;
        this.cropMin.setY(height / 4);
        this.cropMax.setY(3 * height / 4);
    }
}
