package lab3.model;

import lab1.matrix.IMatrix;
import lab1.vector.Vector;

public class Line {

    private Point a;
    private Point b;
    private Point pA;
    private Point pB;

    public Line(Point a, Point b) {
        this.a = a;
        this.b = b;

        double k = ((double)(a.getY() - b.getY())) / (a.getX() - b.getX());
        double deltaY = 4 / (Math.sqrt(k*k + 1));
        double deltaX = Math.abs(k * deltaY);

        if (k < 0) {
            this.pA = new Point((int)(a.getX() + deltaX), (int)(a.getY() + deltaY));
            this.pB = new Point((int)(b.getX() + deltaX), (int)(b.getY() + deltaY));

            if (!isRightFromLine(pA)) {
                this.pA = new Point((int)(a.getX() - deltaX), (int)(a.getY() - deltaY));
                this.pB = new Point((int)(b.getX() - deltaX), (int)(b.getY() - deltaY));
            }
        } else {
            this.pA = new Point((int)(a.getX() + deltaX), (int)(a.getY() - deltaY));
            this.pB = new Point((int)(b.getX() + deltaX), (int)(b.getY() - deltaY));

            if (!isRightFromLine(pA)) {
                this.pA = new Point((int)(a.getX() - deltaX), (int)(a.getY() + deltaY));
                this.pB = new Point((int)(b.getX() - deltaX), (int)(b.getY() + deltaY));
            }
        }
    }

    public Line(int x1, int y1, int x2, int y2) {
        this(new Point(x1, y1), new Point(x2, y2));
    }

    private boolean isRightFromLine(Point p) {
        double ca = a.getY() - b.getY();
        double cb = -(a.getX() - b.getX());
        double cc = -ca * a.getX() + cb * a.getY();

        IMatrix g = new Vector(ca, cb, cc).toColumnMatrix(false);
        IMatrix tp = new Vector(p.getX(), p.getY(), 1).toRowMatrix(false);

        double result = tp.nMultiply(g).get(0, 0);

        if (ca / cb < 0) {
            return result < 0;
        } else {
            return result > 0;
        }
    }

    public Point getA() {
        return a;
    }

    public Point getB() {
        return b;
    }

    public Point getpA() {
        return pA;
    }

    public Point getpB() {
        return pB;
    }

}
