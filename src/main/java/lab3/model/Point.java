package lab3.model;

public class Point {

    private int x;
    private int y;
    private boolean[] code;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
        this.code = new boolean[4];
    }

    public void generateCode(Point pMin, Point pMax) {
        if (y > pMax.getY()) {
            code[0] = true;
        } else {
            code [0] = false;
        }

        if (y < pMin.getY()) {
            code[1] = true;
        } else {
            code [1] = false;
        }

        if (x > pMax.getX()) {
            code[2] = true;
        } else {
            code [2] = false;
        }

        if (x < pMin.getX()) {
            code[3] = true;
        } else {
            code [3] = false;
        }
    }

    public int getFirstTrueInCode() {
        for (int i = 0; i < 4; i++) {
            if (code[i]) {
                return i;
            }
        }

        return -1;
    }

    public Point clone() {
        return new Point(x, y);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean[] getCode() {
        return code;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
}
